fc = 2000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 40;	  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 0;
hzend = 4000;
amp = 0.9;

freqstep = (hzend-hzstart)/length(t);
freq = [hzstart: freqstep: hzend-freqstep];

out1 = cos(2*pi*freq.*t)*amp;

audiowrite("gliss_classe.wav",out1,fc)
