sr      = 48000
ksmps   = 100
nchnls  = 1
0dbfs   = 5






instr 1

kamp linseg 1*p4,p3*0.1,0.8*p4,p3*0.6,0.8*p4,p3*0.1,0.01

aMpoli oscil 0.5, p6, p7
aMunipol=(aMpoli+1)/2
aC oscil  kamp, p5, p8

                out aC*aMunipol



endin

instr 2

kenvfreq1 linseg 0.02, p3/2, 0.5, p3/2, 20
kampmod linseg 1, p3/2, 1, p3/2, 2000
kamp linseg 0, p3/4, 0.05, p3/4, 0.08, p3/4, 0.05, p3/4, 0

afm oscil kampmod, 100,2


afreq oscil 200, kenvfreq1, 1

a1 oscil kamp, 500+afreq+afm, 2
	out a1
endin
