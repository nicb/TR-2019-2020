sr     = 44100
ksmps  = 100
nchnls = 2
0dbfs  = 1

instr 1 
ifile = p5
iamp  = ampdbfs(p4)
iskip = p6

kenv linen iamp,0.05*p3,p3,0.05*p3
aL,aR mp3in ifile, iskip
	outs aL*kenv,aR*kenv
	
endin 

