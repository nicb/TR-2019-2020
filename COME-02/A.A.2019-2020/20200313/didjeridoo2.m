[y fc]=audioread("didjeridoo.wav");
y=y';
start=20000;
nsamples=10000;
dur=length(y)/fc;
binsize=fc/nsamples;
f=[-fc/2:binsize:fc/2-binsize];
sinc=1/fc;
t=[0:sinc:dur-sinc];
yfrag = y(start : start+nsamples-1);
tfrag = t(start : start+nsamples-1);
mydft = zeros(1,nsamples);
for k=1:length(f)
	anal = e.^(-i*2*pi*f(k)*tfrag);
	temp = sum(anal.*yfrag);
	mydft(1,k) = temp;
end	
mag = (abs(mydft)/nsamples)*2;

figure(1)
stem(f,mag)
axis([0 2000])
