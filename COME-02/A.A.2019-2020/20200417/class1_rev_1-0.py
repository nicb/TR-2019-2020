#Compiti prossima volta: modificare orchestra per avere una sonificazione dei dati
#Inviluppo di frequenza con apertura, picco e chiusura
#NON quantizzare le frequenze
#Convertire la data in un AT
#Calcolare la durata per un minutaggio totale di 3 minuti (7 anni -> 3 minuti)

import math

class Nota:
    def __init__ (self, stringa):
        (self.data, self.at, self.dur, self.freq, self.amp, self.table, resto)=stringa.split(',',6)
        self.instr = 3
        self.condizionati()

    def condizionati(self):
        self.at   = float(self.at)
        self.dur  = float(self.dur)
        self.amp  = float(self.amp)
        self.freq = float(self.freq)
        self.table= float(self.table)

    def csound(self):
        print("i%d %8.4f %8.4f %+8.2f %8.4f %d" % (self.instr,self.at, self.dur, self.amp, self.freq, self.table))

class Condizionatore:
    def __init__ (self,nomefile,start=1,stop=-1):
        file=open(nomefile,"r")
        righe=file.readlines()
        file.close()

        self.note=[]
        self.start=start
        self.stop=stop
        for line in righe[self.start:self.stop]:
            self.note.append(Nota(line))

        self.condiziona_il_tutto()

    def manoscritto(self):
        file=open("tsla.sco","w")
        file.write("f1 0 4096 10 1\n")
        for n in self.note[self.start:self.stop]:
            file.write("i%d %8.4f %8.4f %+8.2f %8.4f %d \n" % (n.instr,n.at, n.dur, n.amp, n.freq, n.table))
        file.close()
        print("Score written to file \n")

    def csound(self):
        for n in self.note[self.start:self.stop]:
            n.csound()

    def condiziona_il_tutto(self):
        self.condiziona_gli_at()
        self.condiziona_i_dur()
        self.condiziona_gli_amp()
        self.condiziona_le_freq()
        self.condiziona_le_table()

    def condiziona_gli_at(self):
        min_at=1e10
        for n in self.note[self.start:self.stop]:
            if n.at < min_at:
                min_at=n.at
        for n in self.note[self.start:self.stop]:
            n.at-=min_at

    def condiziona_i_dur(self,min=0.05,max=2):
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.dur < real_min:
                real_min=n.dur                                                  #Finds the maximum
            if n.dur > real_max:
                real_max=n.dur                                                  #Finds the minimum
        norm=max/real_max
        for n in self.note[self.start:self.stop]:
            #print("old dur %8.4f" %(n.dur))                                    #flag
            n.dur=n.dur*norm
            if n.dur==0:
                n.dur=n.dur+min
            #print("new dur %8.4f" %(n.dur) +"\n")                              #flag


    def condiziona_gli_amp(self,min=0.05,max=1):
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.amp < real_min:
                real_min=n.amp                                                  #Finds the maximum
            if n.amp > real_max:
                real_max=n.amp                                                  #Finds the minimum
        norm=(3.14/2)/real_max
        for n in self.note[self.start:self.stop]:
            #print("old amp %8.4f" %(n.amp))                                    #flag
            n.amp=n.amp*norm
            if n.amp==0:
                n.amp=n.amp+min
            #print("inter dur %8.4f" %(n.amp))                                  #flag
            n.amp=math.cos(n.amp)
            #print("new amp %8.4f" %(n.amp) +"\n")                              #flag

    def condiziona_le_freq(self,min=16.35,num_notes=96):
        real_min = 1e10
        real_max = 0
        factor=  2**(1/12)
        for n in self.note[self.start:self.stop]:
            if n.freq < real_min:
                real_min=n.freq                                                 #Finds the maximum
            if n.freq > real_max:
                real_max=n.freq                                                 #Finds the minimum
        norm=num_notes/real_max
        for n in self.note[self.start:self.stop]:
            #print("old freq %8.4f" %(n.freq))                                  #flag
            n.freq=n.freq*norm
            #print("inter freq %8.4f" %(n.freq))                                #flag
            n.freq=min*factor**(n.freq//1)
            #print("new freq %8.4f" %(n.freq) +"\n")                            #flag

    def condiziona_le_table(self,max=1):
        for n in self.note[self.start:self.stop]:
            n.table=1

c = Condizionatore("tsla.txt",1,-1)
c.csound()
c.manoscritto()
