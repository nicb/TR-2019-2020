import random
class Nota:
    def __init__(self,at,dur,freq,amp):
        self.at=at
        self.dur=dur
        self.freq=freq
        self.amp=amp

class Triangle:
    def __init__(self,a,b,c,s,e):
        self.a=a
        self.b=b
        self.c=c
        self.start=s
        self.end=e
        self.nota=[]
        self.num_note=random.randint(5,200)

    def generate(self):
        while n<self.num_note:
            at=self.calc_at()
            dur=self.calc_dur()
            freq=self.calc_freq()
            amp=self.calc_amp()
            self.nota.append(Nota(at,dur,freq,amp))



class TriangleLin(Triangle):
    def generate():
        pass

class TriangleExp(Triangle):
    def generate():
        pass

class TriangleCosin(Triangle):
    def generate():
        pass
