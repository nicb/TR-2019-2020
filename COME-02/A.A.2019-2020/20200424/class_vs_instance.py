class A:
    how_many = 0

    def __init__(self, i):
        self.i = i
        A.how_many += 1;

    def show(self):
        print "%d" % (self.i)

    @classmethod
    def how_many_do_we_have(cls):
        print "%d" % (cls.how_many)

#
# istanze
#
a = A(23)
b = A(16)
c = A(42)
d = A(78)

a.show()
b.show()
c.show()
d.show()

#
# metodo di classe
#
A.how_many_do_we_have()
