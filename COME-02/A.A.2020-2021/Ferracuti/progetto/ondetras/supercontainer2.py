import pdb

from string import Template
import yaml

from PappaPronta.voices.voice import Voice
from PappaPronta.patterns.all_patterns import *
from container2 import Container2

class SuperContainer2(Voice):

    def __init__(self, at, dur, cls, pars = {}, flds = [], se = 0):
        self.sub_events = se
        super().__init__(at, dur, cls, pars, flds)

    def generate(self):
        nevs = self.sub_events

        id = 1
        while (id <= nevs):
            this_dict = self.template_reader(id, self.at)
            this_cls = Container
            msg = "; Container %03d" % (id)
            self.events.append(this_cls.create(this_dict))
            id += 1


    def extract_parameter(self, k, id, at):
        par=self.parameters[k]
        res=None
        if isinstance(par,list):
            res=par[id-1]
        else:
            res=par.next(at)
        return res

    def template_reader(self, id, at):
      template = None
      template_filename = 'container.yml.template'
      with open(template_filename, 'r') as tf:
          template = Template(tf.read())
      values = {
        'this_at' :  self.extract_parameter("ats",id,at)+ at,
        'this_dur' : self.extract_parameter('durs',id,at),
        # parameters
        'id':    id,
        'instr': self.extract_parameter('instr',id,at),
        'this_duration': self.extract_parameter('durations',id,at),
        'this_amposc': self.extract_parameter('amposc',id,at),
        'this_ampmod': self.extract_parameter('ampmod',id,at),
        'this_freqmod': self.extract_parameter('freqmods',id,at),
        'this_frequency': self.extract_parameter('freqosc',id,at),
        # fields
        'field0': self.fields[0],
      }
      yaml_dict = template.substitute(values)
      return yaml.safe_load(yaml_dict)['container']

    #
    # we need to rewrite the create to add the sub-events field
    #
    @classmethod
    def create(cls, dict):
        (at, dur, pars, flds) = cls.parse_dictionary(dict)
        return cls(at, dur, dict['class'], pars, flds, dict['sub_events'])

    #
    # we need to rewrite the check_parameters to be transparent for this class
    #
    def check_parameters(self):
        pass
    #
    # we need to rewrite the parse_dictionary method to be semi-transparent
    #
    @classmethod
    def parse_dictionary(cls, dict):
      at = dict['at']
      dur = dict['dur']
      sub_events = dict['sub_events']
      parameters_to_parse = ['instr', 'ats', 'durs', 'durations']
      sub_dict = { 'at': at, 'dur': dur, 'parameters': {}, 'fields': []}
      for k in parameters_to_parse:
          sub_dict['parameters'][k] = dict['parameters'][k]
      (at, dur, pars, dummy_fields) = super(SuperContainer,cls).parse_dictionary(sub_dict)
      for k,v in pars.items():
          dict['parameters'][k] = v
      return [at, dur, dict['parameters'], dict['fields']]
