fc = 5000;
sinc = 1/fc;
freq = 1000;
dur = 2;
t = [0 : sinc : dur - sinc];
amp = 0.8;
y = amp * e.^(i*2*pi*freq*t);
nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq 
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
mag = abs(ris)/length(t);
stem(F, mag)

	
