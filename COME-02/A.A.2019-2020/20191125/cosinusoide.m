fc = 48000; %freq. di campionamento
sinc = 1/fc; %periodo di camp.
dur = 0.1;  %durata in secondi
freq = 200;  %frequenza in Hz
t = [0:sinc:dur-sinc]; %asse del tempo
y = cos (2*pi*t*freq); 
plot (t,y)
axis ([0.05 0.06 -2 2])

