[y fc]=audioread("trumpet_wav.wav");
y = y'; 

nsamples=1000


dur=length(y)/fc;
sinc=1/fc;
t=[0:sinc:dur-sinc];
binsize=fc/nsamples;
f=[-fc/2:binsize:fc/2-binsize];
yfrag = y(5000 : 5000+nsamples-1);
tfrag = t(5000 : 5000+nsamples-1);

mydft = zeros(1, nsamples);

for k = 1 : length(f)
  anal = e.^(-i*2*pi*f(k)*tfrag);
  inter= anal .* yfrag;
  temp = sum(inter);
  mydft(1,k) = temp;
end

mag = abs(mydft)/nsamples;


figure(1)
stem(f, mag*2)
axis([0 7000])
