sr = 48000
ksmps = 10
nchnls = 1
0dbfs = 1


        instr 1
    iamp = ampdbfs(p4)
    ifreq = p5
    ifn   = 1
    idur  = p3
    ;
    ; amplitude envelope
    ;
    iattack = idur*p6
    idecay  = idur*p7
    irelease = idur*p9
    isusa   = iamp/p8                      ; amplitude
    isust   = idur-iattack-idecay-irelease ; time
    ;
    ; frequency envelope
    ;
    if0t    = idur * ; FIXME: metti numero random compreso tra 0.001 e 0.01
    if0a    = ; FIXME: metti numero random compreso tra -0.7 e 1.3 
    if1t    = idur * ; FIXME: metti numero random compreso tra 0.001 e 0.01
    if1a    = -if0a*0.5
    if2t    = idur * ; FIXME: metti numero random compreso tra 0.001 e 0.01
    if3t    = idur-if0t-if1t-if2t
    ;
    ; vibrato parameters
    ;
    ivibf   = p10   ; vibrato frequency
    iviba   = p11   ; vibrato amplitude


    kamp expseg 0.001, iattack, iamp, idecay, isusa, isust, isusa, irelease, 0.001
    kfreq expseg ifreq, if0t, ifreq*if0a, if1t, ifreq*if1a, if2t, ifreq, if3t, ifreq
    kviba linseg 0, idur/4, 0, idur/4, iviba, idur/2, iviba
    kvibf oscil  kviba, ivbf, ifn
    as  oscili kamp, kfreq+kvibf, ifn

    as  linen  as,dur/100,dur,dur/100
        out    as

        endin
