# Lezione di venerdì 17 luglio 2020 (lezione svolta in remoto via zoom)

<!-- ## [Video della lezione](https://youtu.be/0od4HY7xzhU) -->

## Argomenti

### Composizione Musicale Elettroacustica

* Segmentazioni dello spazio frequenziale - ipotesi:
  * segmentazione in spazi equabili
  * segmentazione in spazi non equabili (naturali, pitagorici, numerici, ecc.)
  * segmentazione spettralista *à la Grisey*
  * segmentazioni *fuzzy* e probabilistiche
* Organizzazione delle scelte sullo spazio frequenziale:
  * organizzazione statistica
  * organizzazione gerarchica
  * organizzazione seriale
  * organizzazione contrappuntistica
* processi formali e tempi lunghi
  * forme contrappuntistiche
  * forme classiche
  * forme contemporanee

## Compiti
