# Lezione di venerdì 27 marzo 2020 (lezione svolta in remoto via zoom)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica (2 ore)

* Correzione in classe della realizzazione prodotta da Francesco Ferracuti
* Strumentario della composizione musicale elettroacustica:
  * illustrazione dello strumento di etichettatura del software di editing `audacity`
  * combinare un linguaggio generico di programmazione (in questo caso:
    `python`) con `csound`

#### [Prima versione della realizzazione (senza correzioni)](./dodda.csd)

```csound
<CsoundSynthesizer>
<CsOptions>
-o "doda3.wav" -W
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1
instr 1
a1 diskin "didjeridoo.wav", p4
out a1
endin

instr 2
kfreq linseg p5, 4, p5, 2, p5*2, 2, p5/2, 2, p5, 4, p5
a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 2*p3, p3, 2*p3
out a1
endin

instr 3
a1 oscil ampdbfs(p4), p5, p6
a1 linen a1, 2*p3, p3, 2*p3
out a1
endin

instr 4
kfreq1 linseg p5, 0.3, p5/4
a1 oscil ampdbfs(p4), kfreq1, p6
a1 linen a1, 0.05*p3, p3, 0.7*p3
out a1
endin
</CsInstruments>
<CsScore>
f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111;quadra
f2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14        
i1 3 20 1
i1 18 7 4
i1 7 15 0.5
i2 8 13 -6 44 1     
i3 2 10 -3 44 1
i2 10 14 -6 88 1
i4 14 2 -12 352 1
i4 14.5 2 -12 176 2
</CsScore>
</CsoundSynthesizer>
```

```sh
$ csound dodda.csd
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
UnifiedCSD:  dodda.csd
STARTING FILE
Creating options
Creating orchestra
closing tag
Creating score
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
graphics suppressed, ascii substituted
0dBFS level = 1.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to doda3.wav (WAV)
SECTION 1:
ftable 1:
ftable 1:	4096 points, scalemax 1.000
   .'-    _''-_     _     .''-    _''
      -_ .     -..-' '...'    '_ .   .
  .     '                       '

                                      -
 _


                                       -
_______________________________________________________________________________


                                        .
                                                                              '


                                         -                                   _
                                               _                       _
                                          _  _' '_    .--._ _.--.     - '_
                                           __     .__-     '     '__.'    ._'
ftable 2:
ftable 2:	4096 points, scalemax 1.000
    -'-
   .   '
        '
  _      '.___...
                 '.
                   '.
 .                   '''''-.
                            '.
                              '-...._
_____________________________________'-________________________________________
                                        -.
                                          ''----.
                                                 '.                           .
                                                   '-.____
                                                          '-_
                                                             .               .
                                                              '---'''-
                                                                      '     _
                                                                       '
                                                                        '__-
B  0.000 ..  2.000 T  2.000 TT  2.000 M:  0.00000
new alloc for instr 3:
WARNING: mp3 too short in linenm
mB  2.000 ..  3.000 T  3.000 TT  3.000 M:  0.03357
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  3.000 ..  7.000 T  7.000 TT  7.000 M:  0.23635
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  7.000 ..  8.000 T  8.000 TT  8.000 M:  0.36266
new alloc for instr 2:
WARNING: mp3 too short in linenm
mB  8.000 .. 10.000 T 10.000 TT 10.000 M:  0.39661
new alloc for instr 2:
WARNING: mp3 too short in linenm
mB 10.000 .. 14.000 T 14.000 TT 14.000 M:  0.44119
new alloc for instr 4:
B 14.000 .. 14.500 T 14.500 TT 14.500 M:  0.48880
new alloc for instr 4:
B 14.500 .. 18.000 T 18.000 TT 18.000 M:  0.65904
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B 18.000 .. 21.000 T 21.000 TT 21.000 M:  0.46267
B 21.000 .. 22.000 T 22.000 TT 22.000 M:  0.39912
B 22.000 .. 23.000 T 23.000 TT 23.000 M:  0.12466
B 23.000 .. 24.000 T 24.000 TT 24.000 M:  0.12530
B 24.000 .. 25.000 T 25.000 TT 25.000 M:  0.00000
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  0.65904
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.050s, CPU: 0.050s
256 512 sample blks of shorts written to doda3.wav (WAV)
```

![dodda0 sonogram](./dodda0_result_shot.png)

Nonostante questo codice compili correttamente e non distorca, esso contiene
numerosi errori concettuali - specialmente nel calcolo dei segmenti temporali
degli inviluppi - che non consentono una realizzazione corretta del frammento
sonoro (i.e.: il frammento contiene numerosi click).

#### [Seconda versione della realizzazione (corretta)](./dodda1.csd)

```csound
<CsoundSynthesizer>
<CsOptions>
-o "dodda31.wav" -W
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1
instr 1
a1 diskin "didjeridoo.wav", p4
out a1
endin

instr 2
kfreq linseg p5, p3*0.3, p5, p3*0.1, p5*2, p3*0.1, p5/2, p3*0.1, p5, p3*0.4, p5
a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 0.5*p3, p3, 0.3*p3
out a1
endin

instr 3
a1 oscil ampdbfs(p4), p5, p6
a1 linen a1, 0.2*p3, p3, 0.05*p3
out a1
endin

instr 4
kfreq1 linseg p5, p3*0.03, p5/4, p3*0.97, p5/4
a1 oscil ampdbfs(p4), kfreq1, p6
a1 linen a1, 0.05*p3, p3, 0.07*p3
out a1
endin
</CsInstruments>
<CsScore>
f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111
f2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14        
i1 3 20 1
i1 18 7 4
i1 7 15 0.5
i2 8 13 -6 44 1     
i3 2 10 -3 44 1
i2 10 14 -6 88 1
i4 14 2 -12 352 1
i4 14.5 2 -12 176 2
</CsScore>
</CsoundSynthesizer>
```

```sh
$ csound dodda1.csd
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
UnifiedCSD:  dodda1.csd
STARTING FILE
Creating options
Creating orchestra
closing tag
Creating score
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
graphics suppressed, ascii substituted
0dBFS level = 1.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to dodda31.wav (WAV)
SECTION 1:
ftable 1:
ftable 1:	4096 points, scalemax 1.000
   .'-    _''-_     _     .''-    _''
      -_ .     -..-' '...'    '_ .   .
  .     '                       '

                                      -
 _


                                       -
_______________________________________________________________________________


                                        .
                                                                              '


                                         -                                   _
                                               _                       _
                                          _  _' '_    .--._ _.--.     - '_
                                           __     .__-     '     '__.'    ._'
ftable 2:
ftable 2:	4096 points, scalemax 1.000
    -'-
   .   '
        '
  _      '.___...
                 '.
                   '.
 .                   '''''-.
                            '.
                              '-...._
_____________________________________'-________________________________________
                                        -.
                                          ''----.
                                                 '.                           .
                                                   '-.____
                                                          '-_
                                                             .               .
                                                              '---'''-
                                                                      '     _
                                                                       '
                                                                        '__-
B  0.000 ..  2.000 T  2.000 TT  2.000 M:  0.00000
new alloc for instr 3:
B  2.000 ..  3.000 T  3.000 TT  3.000 M:  0.17710
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  3.000 ..  7.000 T  7.000 TT  7.000 M:  0.48261
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  7.000 ..  8.000 T  8.000 TT  8.000 M:  0.56889
new alloc for instr 2:
B  8.000 .. 10.000 T 10.000 TT 10.000 M:  0.59702
new alloc for instr 2:
B 10.000 .. 14.000 T 14.000 TT 14.000 M:  0.67324
new alloc for instr 4:
B 14.000 .. 14.500 T 14.500 TT 14.500 M:  0.61594
new alloc for instr 4:
B 14.500 .. 18.000 T 18.000 TT 18.000 M:  0.76089
new alloc for instr 1:
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B 18.000 .. 21.000 T 21.000 TT 21.000 M:  0.65879
B 21.000 .. 22.000 T 22.000 TT 22.000 M:  0.38173
B 22.000 .. 23.000 T 23.000 TT 23.000 M:  0.11957
B 23.000 .. 24.000 T 24.000 TT 24.000 M:  0.05977
B 24.000 .. 25.000 T 25.000 TT 25.000 M:  0.00000
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  0.76089
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.051s, CPU: 0.051s
256 512 sample blks of shorts written to dodda31.wav (WAV)
```

![dodda31 sonogram](./dodda31_result_shot.png)

#### Illustrazione dello strumento di etichettatura del software di editing `audacity`

![audacity label track](./didjeridoo_with_labels_shot.png)

La funzione di export produce il seguente testo in codice *ASCII puro* (i.e.
modificabile con un qualsiasi editor di testo tipo `atom`, `gedit`, ecc.):

```text
0.248983	3.997560	prima parte
6.957690	12.822622	seconda parte
```

#### [Combinazione di uno linguaggio di programmazione generico (`python`) con `csound`](./primo.py)

```python
#python
from random import randint, random
n_note=100
n=0
at=0
print("f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111\nf2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14")
while (n<n_note):
    instr=randint(1,4)
    step=random()*0.2
    dur=random()*0.5+0.10
    freq=random()*200+150
    amp=random()*-6-12
    fun=randint(1,2)
    print("i%d %8.4f %8.4f %+8.4f %8.4f %d" % (instr, at, dur, amp, freq, fun))
    at=at+step
    n=n+1
```

Questo programma, eseguito, produce cento note nel linguaggio di partitura
di `csound`

```csound
$ python primo.py
f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111
f2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14
i3   0.0000   0.2395 -12.7468 245.3533 2
i2   0.1425   0.3034 -16.1622 337.9520 1
i2   0.3348   0.1725 -13.0322 274.8631 1
i3   0.4109   0.1886 -13.8365 246.5537 1
i3   0.4668   0.2621 -16.9538 299.1354 2
i3   0.6500   0.2422 -14.9655 209.6498 2
i2   0.8170   0.1037 -12.3171 260.1745 1
i1   0.8773   0.3283 -13.1063 179.0695 1
i1   0.8990   0.4128 -17.9029 175.3932 2
i4   1.0390   0.1210 -17.4581 295.3626 1
i4   1.0583   0.2401 -16.1341 297.9463 1
i1   1.2060   0.1443 -15.2170 203.5284 1
i2   1.2874   0.2997 -17.2458 211.7929 1
i4   1.4305   0.4683 -17.4072 260.5605 1
i4   1.5230   0.3628 -16.4376 188.3446 1
i2   1.6285   0.2142 -12.2184 260.0666 2
i4   1.8003   0.3633 -17.0434 160.3804 1
i3   1.9802   0.3376 -17.2671 248.5845 1
i1   2.0786   0.4668 -15.9111 195.4998 1
i2   2.2541   0.5752 -16.2964 327.6587 1
i3   2.4502   0.2353 -12.3296 312.0788 2
i2   2.5712   0.3986 -17.1894 344.9472 1
i2   2.6510   0.5300 -15.5714 243.7185 1
i3   2.7392   0.3787 -15.6336 291.2854 1
i3   2.8798   0.4341 -12.1455 286.4301 2
i4   2.9060   0.5250 -12.4208 307.2115 2
i2   2.9413   0.4527 -13.8492 202.7654 2
i3   3.1077   0.1434 -17.6315 269.5505 2
i2   3.1874   0.1066 -15.1803 347.4910 1
i2   3.3545   0.5344 -17.0604 233.9891 2
i2   3.4645   0.4202 -13.4825 329.6219 2
i4   3.5312   0.2339 -13.3429 184.8702 2
i2   3.7166   0.1089 -15.6513 288.0703 2
i2   3.8312   0.1026 -17.3218 191.6042 1
i1   4.0173   0.1883 -13.9795 181.6732 2
i2   4.0936   0.5418 -17.6680 312.7488 2
i2   4.1567   0.1224 -13.1509 150.1863 2
i4   4.2930   0.5731 -15.8412 167.3535 1
i4   4.4591   0.3730 -12.3524 304.9866 1
i1   4.5190   0.3017 -12.1270 337.4482 2
i4   4.6420   0.5419 -17.5895 286.2278 1
i4   4.7305   0.4181 -16.0992 341.3844 2
i2   4.7566   0.4501 -15.6694 330.9950 2
i4   4.8730   0.2407 -14.3562 234.6395 2
i4   4.9846   0.2030 -14.4612 185.5639 2
i2   5.0666   0.2267 -12.2373 225.3024 2
i3   5.2495   0.3599 -14.0994 262.6401 1
i4   5.3012   0.4002 -15.8908 272.7433 1
i3   5.3314   0.1006 -15.1576 176.7439 1
i2   5.5284   0.2212 -14.4983 213.8319 2
i3   5.6528   0.3833 -13.3037 156.8042 1
i2   5.8402   0.4669 -15.1898 204.2169 2
i4   5.8559   0.2265 -12.6164 239.2604 2
i2   5.9487   0.3178 -14.8560 193.5034 1
i1   6.0105   0.3864 -15.1270 233.8849 2
i4   6.1324   0.4648 -13.6050 229.1384 2
i4   6.2997   0.5411 -17.5052 198.3806 1
i4   6.3627   0.4012 -14.8764 202.6279 1
i4   6.4406   0.4573 -17.5503 156.7868 2
i4   6.6269   0.5681 -16.5835 198.1353 1
i4   6.7758   0.2777 -17.1747 340.0898 1
i3   6.8454   0.1540 -14.3297 320.8661 1
i2   6.9857   0.4812 -17.5049 190.8847 1
i4   7.1097   0.4968 -13.8868 326.2709 2
i3   7.2003   0.4508 -17.5595 258.3772 2
i3   7.3566   0.4925 -16.2573 190.0504 2
i4   7.5415   0.1709 -13.0668 166.7100 1
i2   7.7075   0.3837 -17.5466 274.9618 1
i3   7.8712   0.4119 -12.8998 281.8666 2
i2   7.9157   0.4501 -14.3515 346.7147 2
i2   7.9585   0.5511 -17.9534 209.3585 2
i1   8.0750   0.5001 -15.7384 164.7407 2
i1   8.2575   0.4094 -16.8684 222.9823 2
i1   8.3746   0.2736 -16.8529 315.1545 2
i1   8.5617   0.4380 -13.9477 305.0128 1
i4   8.7291   0.2389 -13.0272 177.5244 2
i3   8.8924   0.1124 -12.1250 259.9993 2
i2   8.9459   0.5746 -13.3548 168.9573 1
i4   9.0921   0.1307 -15.8940 160.1271 2
i1   9.2567   0.3070 -14.5323 153.8339 1
i1   9.2973   0.4548 -12.6362 326.5642 1
i2   9.3897   0.4588 -15.7734 160.7026 1
i1   9.5195   0.5487 -16.7922 278.6091 1
i4   9.5707   0.4405 -17.6533 286.1627 1
i1   9.7565   0.3276 -13.9748 205.1725 1
i3   9.8341   0.1075 -13.7433 206.5149 1
i3   9.9307   0.5533 -15.9177 343.9664 1
i3   9.9853   0.3079 -17.2533 211.6946 2
i2  10.0347   0.3920 -17.9341 242.7859 2
i3  10.2248   0.2883 -13.5783 209.1099 1
i2  10.4208   0.5791 -13.2458 213.3789 2
i3  10.6208   0.2073 -14.1233 209.6851 2
i1  10.6574   0.5577 -16.7171 161.7255 1
i1  10.7410   0.3100 -16.8278 328.9585 2
i3  10.7845   0.1217 -13.7345 185.7852 1
i3  10.8579   0.3017 -16.9944 220.3213 2
i1  10.9647   0.5825 -17.8466 274.7799 2
i4  11.0498   0.3168 -13.3907 343.7617 2
i2  11.0862   0.4420 -13.7908 228.7479 2
i1  11.1406   0.5780 -16.2165 349.0766 2
```

Quando l'esecuzione viene ridiretta in un file (`python primo.py > primo.sco`)
esso può essere poi combinato con [un file orchestra `csound`](./driver.orc), come ad esempio:

```csound
sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1
instr 1
a1 diskin "didjeridoo.wav", p4
out a1
endin

instr 2
kfreq linseg p5, p3*0.2, p5, p3*01, p5*2, p3*0.1, p5/2, p3*0.1, p5, p3*0.2, p5
a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 0.2*p3, p3, 0.5*p3
out a1
endin

instr 3
a1 oscil ampdbfs(p4), p5, p6
a1 linen a1, 0.2*p3, p3, 0.1*p3
out a1
endin

instr 4
kfreq1 linseg p5, p3*0.03, p5/4, p3*0.97, p5/4
a1 oscil ampdbfs(p4), kfreq1, p6
a1 linen a1, 0.05*p3, p3, 0.7*p3
out a1
endin
```

e il risultato sarà:

```sh
$ csound -dWo pysound.wav driver.orc primo.sco
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
orchname:  driver.orc
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
displays suppressed
0dBFS level = 1.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to pysound.wav (WAV)
SECTION 1:
ftable 1:
ftable 2:
new alloc for instr 3:
B  0.000 ..  0.198 T  0.197 TT  0.197 M:  0.16411
new alloc for instr 3:
B  0.198 ..  0.240 T  0.239 TT  0.239 M:  0.18366
new alloc for instr 1:
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  0.240 ..  0.309 T  0.309 TT  0.309 M:  0.23564
new alloc for instr 1:
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  0.309 ..  0.430 T  0.430 TT  0.430 M:  0.27678
B  0.430 ..  0.612 T  0.612 TT  0.612 M:  0.32469
new alloc for instr 1:
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  0.612 ..  0.805 T  0.805 TT  0.805 M:  0.19561
new alloc for instr 4:
B  0.805 ..  0.903 T  0.903 TT  0.903 M:  0.18460
new alloc for instr 4:
B  0.903 ..  0.992 T  0.992 TT  0.992 M:  0.38451
B  0.992 ..  1.110 T  1.110 TT  1.110 M:  0.36898
B  1.110 ..  1.303 T  1.303 TT  1.303 M:  0.48187
new alloc for instr 2:
B  1.303 ..  1.375 T  1.375 TT  1.375 M:  0.33749
new alloc for instr 2:
B  1.375 ..  1.524 T  1.525 TT  1.525 M:  0.28815
B  1.524 ..  1.554 T  1.554 TT  1.554 M:  0.20227
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  1.554 ..  1.633 T  1.633 TT  1.633 M:  0.33107
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  1.633 ..  1.820 T  1.820 TT  1.820 M:  0.31833
new alloc for instr 2:
B  1.820 ..  1.875 T  1.874 TT  1.874 M:  0.12149
B  1.875 ..  2.023 T  2.023 TT  2.023 M:  0.35660
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  2.023 ..  2.208 T  2.208 TT  2.208 M:  0.35653
B  2.208 ..  2.270 T  2.270 TT  2.270 M:  0.34713
B  2.270 ..  2.415 T  2.415 TT  2.415 M:  0.36667
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  2.415 ..  2.453 T  2.453 TT  2.453 M:  0.33169
B  2.453 ..  2.455 T  2.456 TT  2.456 M:  0.10710
new alloc for instr 4:
B  2.455 ..  2.624 T  2.624 TT  2.624 M:  0.58632
B  2.624 ..  2.720 T  2.720 TT  2.720 M:  0.60121
B  2.720 ..  2.870 T  2.870 TT  2.870 M:  0.65168
B  2.870 ..  3.065 T  3.065 TT  3.065 M:  0.50010
B  3.065 ..  3.174 T  3.175 TT  3.175 M:  0.44417
B  3.174 ..  3.217 T  3.217 TT  3.217 M:  0.49571
new alloc for instr 3:
B  3.217 ..  3.264 T  3.264 TT  3.264 M:  0.51727
B  3.264 ..  3.284 T  3.283 TT  3.283 M:  0.50955
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  3.284 ..  3.311 T  3.311 TT  3.311 M:  0.54389
B  3.311 ..  3.345 T  3.345 TT  3.345 M:  0.53549
B  3.345 ..  3.431 T  3.431 TT  3.431 M:  0.51393
B  3.431 ..  3.476 T  3.476 TT  3.476 M:  0.35643
B  3.476 ..  3.520 T  3.520 TT  3.520 M:  0.46640
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  3.520 ..  3.561 T  3.561 TT  3.561 M:  0.45060
B  3.561 ..  3.594 T  3.594 TT  3.594 M:  0.47072
B  3.594 ..  3.661 T  3.661 TT  3.661 M:  0.46036
B  3.661 ..  3.701 T  3.701 TT  3.701 M:  0.38757
B  3.701 ..  3.836 T  3.836 TT  3.836 M:  0.43382
B  3.836 ..  3.911 T  3.911 TT  3.911 M:  0.34915
B  3.911 ..  3.964 T  3.964 TT  3.964 M:  0.33788
B  3.964 ..  4.075 T  4.074 TT  4.074 M:  0.46534
B  4.075 ..  4.178 T  4.178 TT  4.178 M:  0.55253
B  4.178 ..  4.293 T  4.293 TT  4.293 M:  0.33755
B  4.293 ..  4.413 T  4.413 TT  4.413 M:  0.41365
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  4.413 ..  4.547 T  4.547 TT  4.547 M:  0.27576
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  4.547 ..  4.712 T  4.711 TT  4.711 M:  0.06378
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  4.712 ..  4.742 T  4.742 TT  4.742 M:  0.00003
B  4.742 ..  4.912 T  4.912 TT  4.912 M:  0.23546
B  4.912 ..  5.030 T  5.029 TT  5.029 M:  0.20296
B  5.030 ..  5.129 T  5.129 TT  5.129 M:  0.41457
B  5.129 ..  5.238 T  5.238 TT  5.238 M:  0.54199
B  5.238 ..  5.302 T  5.302 TT  5.302 M:  0.50113
B  5.302 ..  5.321 T  5.321 TT  5.321 M:  0.43904
B  5.321 ..  5.454 T  5.454 TT  5.454 M:  0.60904
B  5.454 ..  5.605 T  5.605 TT  5.605 M:  0.59417
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  5.605 ..  5.669 T  5.669 TT  5.669 M:  0.32196
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  5.669 ..  5.689 T  5.690 TT  5.690 M:  0.30204
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  5.689 ..  5.702 T  5.702 TT  5.702 M:  0.25335
B  5.702 ..  5.873 T  5.873 TT  5.873 M:  0.29712
B  5.873 ..  6.010 T  6.010 TT  6.010 M:  0.32720
B  6.010 ..  6.125 T  6.124 TT  6.124 M:  0.34703
B  6.125 ..  6.130 T  6.130 TT  6.130 M:  0.25904
new alloc for instr 2:
B  6.130 ..  6.190 T  6.190 TT  6.190 M:  0.32580
B  6.190 ..  6.285 T  6.285 TT  6.285 M:  0.40001
B  6.285 ..  6.353 T  6.353 TT  6.353 M:  0.36708
B  6.353 ..  6.488 T  6.489 TT  6.489 M:  0.38111
B  6.488 ..  6.628 T  6.628 TT  6.628 M:  0.35217
B  6.628 ..  6.757 T  6.757 TT  6.757 M:  0.39335
B  6.757 ..  6.916 T  6.916 TT  6.916 M:  0.30537
B  6.916 ..  6.963 T  6.962 TT  6.962 M:  0.42174
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  6.963 ..  7.022 T  7.022 TT  7.022 M:  0.39240
B  7.022 ..  7.162 T  7.163 TT  7.163 M:  0.44097
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  7.162 ..  7.285 T  7.285 TT  7.285 M:  0.04446
B  7.285 ..  7.469 T  7.469 TT  7.469 M:  0.17484
B  7.469 ..  7.626 T  7.626 TT  7.626 M:  0.15282
B  7.626 ..  7.658 T  7.658 TT  7.658 M:  0.08953
B  7.658 ..  7.856 T  7.856 TT  7.856 M:  0.30087
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  7.856 ..  8.040 T  8.041 TT  8.041 M:  0.03025
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  8.040 ..  8.165 T  8.165 TT  8.165 M:  0.00003
B  8.165 ..  8.349 T  8.349 TT  8.349 M:  0.15309
B  8.349 ..  8.493 T  8.493 TT  8.493 M:  0.18864
B  8.493 ..  8.638 T  8.638 TT  8.638 M:  0.35398
B  8.638 ..  8.770 T  8.770 TT  8.770 M:  0.57001
B  8.770 ..  8.885 T  8.885 TT  8.885 M:  0.60761
B  8.885 ..  9.066 T  9.066 TT  9.066 M:  0.46776
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  9.066 ..  9.170 T  9.170 TT  9.170 M:  0.02742
B  9.170 ..  9.330 T  9.329 TT  9.329 M:  0.14837
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  9.330 ..  9.366 T  9.366 TT  9.366 M:  0.14837
B  9.366 ..  9.473 T  9.473 TT  9.473 M:  0.33569
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B  9.473 ..  9.617 T  9.617 TT  9.617 M:  0.31885
B  9.617 ..  9.786 T  9.786 TT  9.786 M:  0.18367
B  9.786 ..  9.948 T  9.948 TT  9.948 M:  0.25160
B  9.948 .. 10.129 T 10.130 TT 10.130 M:  0.18162
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B 10.129 .. 10.220 T 10.220 TT 10.220 M:  0.14923
B 10.220 .. 10.297 T 10.297 TT 10.297 M:  0.20716
WARNING: instr 1 uses 4 p-fields but is given 6
diskin2: opened 'didjeridoo.wav':
         44100 Hz, 1 channel(s), 666129 sample frames

B 10.297 .. 10.481 T 10.481 TT 10.481 M:  0.19602
B 10.481 .. 10.633 T 10.633 TT 10.633 M:  0.24287
B 10.633 .. 10.807 T 10.807 TT 10.807 M:  0.19922
B 10.807 .. 10.896 T 10.897 TT 10.897 M:  0.13954
B 10.896 .. 11.209 T 11.209 TT 11.209 M:  0.13953
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  0.65168
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.027s, CPU: 0.027s
256 512 sample blks of shorts written to pysound.wav (WAV)
```

Il risultato (che varierà da computer a computer e da esecuzione a esecuzione
del programma), potrà esssere qualcosa del tipo illustrato qui sotto:

![pysound screenshot](./pysound_result_shot.png)

## Compiti per casa

* utilizzare il programma [`primo.py`](./primo.py) variandone i parametri
  per realizzare versioni diverse del frammento
