import random
class Nota:
    def __init__(self,at,dur,freq,amp,instr):
        self.at=at
        self.dur=dur
        self.amp=amp
        self.freq=freq
        self.instr=instr

    def to_csound(self):
        a="i%d %8.4f %8.4f %8.4f %8.4f\n" %(self.instr,self.at,self.dur,self.freq,self.amp)
        return a

class Generatore:
    def __init__(self,num_note,at,dur,fmin,fmax):
        self.at=at
        self.dur=dur
        self.fmin=fmin
        self.fmax=fmax
        self.note=[]
        self.num_note=num_note
    def generate(self):
        for x in range(1,self.num_note):
            at=self.calc_at()
            dur=self.calc_dur()
            freq=self.calc_freq()
            amp=self.calc_amp()
            instr=1
            n=Nota(at,dur,freq,amp,instr)
            self.note.append(n)
    def calc_at(self):
        at=random.random()*self.dur+self.at
        return at
    def calc_dur(self):
        dur=0.5
        return dur
    def calc_freq(self):
        r=self.fmax-self.fmin
        freq=random.random()*r+self.fmin
        return freq
    def calc_amp(self):
        amp=-24
        return amp
    def to_csound(self):
        str=""
        for x in self.note:
            str+=x.to_csound()
        return str
class Composizione:
    def __init__(self,file):
        self.file=file
        self.generatori=[]
        self.create()

    def create(self):
        f=open(self.file,"r")
        lines=f.readlines()
        for l in lines:
            (nn,at,dur,fmin,fmax)=l.split(" ")
            self.generatori.append(Generatore(int(nn),float(at),float(dur),float(fmin),float(fmax)))
        f.close()

    def to_csound(self):
        print("f1 0 4096 10 1")
        for g in self.generatori:
            g.generate()
            print(g.to_csound())
c=Composizione("file.meta")
c.to_csound()
