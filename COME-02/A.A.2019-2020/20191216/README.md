# Lezione di lunedì 16 dicembre 2019

## Argomenti

* Correzione dei compiti
* Risposta alle domande
* Altro parametro del suono: *la fase*

## Lavagna

![whiteboard 1](./TR_I_2019-12-16_19.27.05_1.jpg)

## Realizzazioni [`octave`](https://octave.org)

### [Risposta al compito - creare un glissando cosinusoidale](./glissando.m)

```matlab
fc = 48000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 0.1;  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 500;
hzend = 3000;

out1 = zeros(length(t)*(hzend-hzstart),1);
y = zeros(length(t),1);
fase = 0;
amp = 0.7;

for k = hzstart:hzend
	globstart = (length(y)*(k-hzstart)) + 1;
	freq = k;
	y = cos(2*pi*t*freq+fase) * amp;
	%fase = (n° di giri) - floor(n°di giri)
	out1(globstart : globstart + length(y)-1) = y ; 
	
end
audiowrite("gliss.wav",out1,fc)

totdur = [0:sinc:dur*(hzend-hzstart+1)-sinc];
```

Questa soluzione crea effettivamente una crescita frequenziale a scalini, però
l'incongruenza di fase tra uno scalino e il seguente genera degli artefatti
sonori perfettamente udibili. Andando a vedere la forma d'onda in dettaglio,
si possono notare tali discontinuità:

![Risposta al compito - frammenti dei confini di cambio frequenza](./glissando_frammenti.jpg)

### [Funzione `chirp` realizzata in classe - non corretta](./glissando_classe.m)

```matlab
fc = 2000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 40;	  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 0;
hzend = 4000;
amp = 0.9;

freqstep = (hzend-hzstart)/length(t);
freq = [hzstart: freqstep: hzend-freqstep];

out1 = cos(2*pi*freq.*t)*amp;

audiowrite("gliss_classe.wav",out1,fc)
```

Così realizzata, la funzione di `chirp` (*glissando*) non restituisce i valori desiderati.
Ciò è dovuto al fatto che abbiamo inserito la dimensione del tempo in due
funzioni che sono imbricate una dentro l'altra (dentro il vettore `freq` e
dentro il calcolo della funzione d'onda `out1`). Osservando lo spettrogramma
della funzione risultante, noteremo che la frequenza si muove al doppio della
velocità desiderata:

![Spettrogramma di `glissando_classe.m`](./glissando_classe.jpg)

### [Funzione `chirp` corretta](./glissando_classe_giusto.m)

Per correggere il problema di cui sopra è necessario considerare che la
funzione d'onda viene generato da una funzione cosinusoidale (`cos`) che
calcola per ogni campione la fase istantanea al tempo *t*, ossia ![$\phi ( t )$](./eqns/phi_t.gif).
Considerando che la frequenza ![$\omega$](./eqns/omega.gif) è la derivata prima della
fase ![$\omega ( t ) = \frac{d \phi ( t)}{d t}$](./eqns/omega_as_derivative.gif),
se la frequenza cambia nel tempo con una funzione lineare ![$\omega ( t ) = c t + \omega_0$](./eqns/linear_frequency_function.gif)
dove ![$c = \frac{\omega_T - \omega_0}{T}$](./eqns/what_is_c.gif), per avere
la fase istantanea in ogni momento sarà necessario integrare questa funzione
nel tempo, ossia ![$\phi ( t ) = \phi_0 + \int_{t_0}^{T}{\omega ( t ) dt} = \phi_0 + \int_{t_0}^{T}{( c \tau + \omega_0 ) d\tau} = \phi_0 + ( \frac{c}{2} t^2 + \omega_0)$](./eqns/integrate_phi.gif).
La funzione generatrice sarà quindi ![$y ( t ) = cos \left [ \phi_0 + (\frac{c}{2} t^2 + \omega_0 t) \right ]$](./eqns/final_function.gif).

Il codice deve essere quindi corretto come segue:

```matlab
fc = 2000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 40;	  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 0;
hzend = 4000;
amp = 0.9;

%
% Come illustrato qui: https://en.wikipedia.org/wiki/Chirp
% la funzione di un chirp lineare è come segue:
%
% y(t) = cos(2*pi*((c/2)*(t**2) + f0)+phase0)
%
% dove:
%
% * phase0 è la fase iniziale
% * c è il tasso di crescita (lineare) della frequenza, dato da: c = (f1-f0)/dur
%
% il codice che segue viene quindi modificato per implementare questa
% funzione.

c = (hzend-hzstart)/dur;
phase0 = 0;
freqfun = (c/2)*(t.**2) + (hzstart*t);

out1 = cos(2*pi*freqfun+phase0)*amp;

audiowrite("gliss_classe_giusto.wav",out1,fc)
```
Questo codice produce lo spettrogramma:

![Spettrogramma di `glissando_classe_giusto.m`](./glissando_classe_corretto_spectrogram.jpg)

### [Fase come combinazione di seni e coseni - formula della somma di angoli](./about_fase.m)

Abbiamo verificato che la funzione ![$y ( t ) = cos ( \omega t + \phi )$](./eqns/werner_start.gif)
è equivalente a ![$y ( t ) = cos ( \phi ) cos ( \omega t ) - sin ( \phi ) sin ( \omega t )$](./eqns/werner_end.gif)

```matlab
fc = 20000;
dur = 0.2;
sinc = 1/fc;

t = [0: sinc: dur-sinc];

fase = pi/4.2;
freq = 100;

y1 = cos(freq*2*pi*t+fase);
y2 = cos(fase)*cos(freq*2*pi*t)-sin(fase)*sin(freq*2*pi*t); 		%formula di Werner

subplot(2,1,1)
plot(t,y1)

subplot(2,1,2)
plot(t,y2)
```

Questo script produce i seguenti plot:

![Werner formula at work](./about_fase.jpg)

## Compiti per casa

* realizzare la prima [risposta al compito](./glissando.m) senza discontinuità di fase
