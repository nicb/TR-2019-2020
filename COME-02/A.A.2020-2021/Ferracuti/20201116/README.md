# COME/02 - Lezione del 16/11/2021 (Francesco Ferracuti)

## Argomenti

* descrizione del programma `pic` con [esempi](./pic/README.md)
* elaborazione di un nuovo sotto-oggetto sonoro in grado di produrre eventi
  multipli (configurati come unico evento "ricco")
* evoluzione della classe generatrice per accogliere multipli sotto-oggetti

## Consegna

* studio di `pic`
* realizzazione del software dedicato
* realizzazione del rendering sonoro
* realizzazione del rendering grafico
