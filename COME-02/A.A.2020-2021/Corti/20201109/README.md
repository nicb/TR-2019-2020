# COME/02 - Lezione del 09/11/2021 (Gabriele Corti)

## Argomenti

* discussione del progetto per il lavoro d'esame (sessione invernale)
* lavoro acusmatico
* principi di variazione continua con elementi di contrasto/disturbo

## Riferimenti

* Brahms (principi di variazione continua)
* Berio - *Formazioni* (elementi indifferenti/a contrasto)

## Consegna

* elaborare progetto generale di forma
* elaborare elementi primari in variazione continua
* elaborare elementi di disturbo/indifferenti
