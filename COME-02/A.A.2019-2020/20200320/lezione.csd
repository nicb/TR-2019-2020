<CsoundSynthesizer>
<CsOptions>
-Wo "lezione0.wav"
</CsOptions>
<CsInstruments>

sr     = 44100
ksmps  = 32
nchnls = 1
0dbfs  = 1

instr 1
ifreq = p4
iamp  = p5
a1 soundin "signal.wav"
asint oscil iamp, ifreq, 1
asint linen asint, p3*0.3, p3, p3*0.05             		 
out (a1+asint)/2
endin

</CsInstruments>
<CsScore>
f1 0 4096 10 1
i1 0 2 602.8 0.7

</CsScore>
</CsoundSynthesizer>
