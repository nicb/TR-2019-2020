# Lezione di venerdì 12 giugno 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/c00FaT7v0vU)

## Argomenti

* Realizzazione di un esempio semplice di segmentazione dello spazio
  frequenziale (proposto da Francesco Ferracuti)
* Discussione dell'esempio semplice

## [Esempio `python`](./randomnotes_3.py)

```python
import random
import re 
class Nota:
    def __init__(self,at,dur,freq,amp,instr):
        self.at=at
        self.dur=dur
        self.freq=freq
        self.amp=amp
        self.instr=instr
        
    def to_csound(self):
        a= "i%d %8.4f %8.4f %8.4f %8.4f \n"%(self.instr,self.at,self.dur,self.freq,self.amp)
        return a 
        
class Generatore:
    def __init__(self,num_note,at,dur,fmin,fmax):
        self.num_note=num_note
        self.at=at
        self.dur=dur
        self.fmin=fmin
        self.fmax=fmax 
        self.note=[]
        self.frequenze=self.genera_frequenze()
        
    def generate(self):
        for x in range(1,self.num_note):
            at =   self.calc_at()
            dur=   self.calc_dur(at)
            freq=  self.calc_freq()
            amp=   self.calc_amp()
            instr= 1 
            n= Nota(at,dur,freq,amp,instr)
            self.note.append(n)
    
    def genera_frequenze(self):
        result=[self.fmin]
        frange=self.fmax-self.fmin
        for n in range(1,4):
            result.append(self.fmin+(frange*(n/4.0)))
        result.append(self.fmax)
        return result
    
    def calc_at(self):                                    
        at=random.uniform(self.at,self.at+self.dur)
        return at
    def calc_dur(self,at):
        dur=random.uniform(at,self.at+self.dur)
        return dur
    def calc_freq(self):
        sz=len(self.frequenze)
        freq=self.frequenze[random.randint(0,sz-1)]
        return freq
    def calc_amp (self):
        amp=-30
        return amp 
    def to_csound(self):
        str="" 
        for x in self.note:
            str+=x.to_csound()
        return str
    
class Composizione :
    def __init__(self, file):
        self.file=file
        self.generatori=[]
        self.create()
    def create (self):
        f=open(self.file,"r")
        lines=f.readlines()
        f.close()
        for j in lines:
            if re.match(r'^\s*#',j):                           #^=inizio riga / *=qualsiasi numero di carattere spazio
                continue
            (nn,at,dur,fmin,fmax)=j.split(" ")
            self.generatori.append(Generatore(int(nn),float(at),float(dur),float(fmin),float(fmax)))
    def to_csound(self):
        print("f1 0 4096 10 1")
        for g in self.generatori:
            g.generate()
            print(g.to_csound())
            
c=Composizione("file.meta") 
c.to_csound() 
```

File di metadati utilizzato:

```
200 0 30 500 500
200 5 25 400 600
100 7 30 300 300
100 12 25 200 400
400 14 30 840 840
400 19 25 740 940
```

Orchestra `csound` utilizzata:

```csound
sr=44100
ksmps=10
nchnls=1

instr 1
iamp=ampdbfs(p5)
ifreq=p4
idur=p3

kenv expseg iamp*0.01,idur*0.1,iamp,idur*0.99,iamp*0.001
aout oscil kenv,ifreq,1
     out aout
endin
```

Questa combinazione produce un brano dallo spettrogramma che segue:

![spettrogramma test](./test.png)

## Compiti

* Realizzare un breve frammento sonoro con spazi frequenziali segmentati a
  piacere (ma *non* a caso). Il lavoro dev'essere corredato di:
  * file orchestra
  * generatore *python*
  * file risultante
  * **TESTO ANALITICO** delle operazioni fatte
