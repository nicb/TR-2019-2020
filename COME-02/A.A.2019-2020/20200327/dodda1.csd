<CsoundSynthesizer>
<CsOptions>
-o "dodda31.wav" -W
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1
instr 1
a1 diskin "didjeridoo.wav", p4
out a1
endin 

instr 2
kfreq linseg p5, p3*0.3, p5, p3*0.1, p5*2, p3*0.1, p5/2, p3*0.1, p5, p3*0.4, p5 
a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 0.5*p3, p3, 0.3*p3
out a1
endin

instr 3
a1 oscil ampdbfs(p4), p5, p6
a1 linen a1, 0.2*p3, p3, 0.05*p3
out a1
endin

instr 4
kfreq1 linseg p5, p3*0.03, p5/4, p3*0.97, p5/4
a1 oscil ampdbfs(p4), kfreq1, p6
a1 linen a1, 0.05*p3, p3, 0.07*p3
out a1
endin
</CsInstruments>
<CsScore>
f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111
f2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14        
i1 3 20 1
i1 18 7 4
i1 7 15 0.5
i2 8 13 -12 44 1     
i3 2 10 -9 44 1
i2 10 14 -12 88 1
i4 14 2 -18 352 1 
i4 14.5 2 -18 176 2 
</CsScore>
</CsoundSynthesizer>
