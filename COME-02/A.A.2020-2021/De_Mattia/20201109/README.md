# COME/02 - Lezione del 09/11/2021 (Giulio Romano Di Mattia)

## Argomenti

* discussione del progetto per il lavoro d'esame (sessione invernale)
* lavoro acusmatico
* forma tripartita `ABA'` con ulteriore suddivisione in
  * `1 - 1.1 1.2 1.1`  
* le parti *uno* sono scritte e definite, le parti *due* sono "improvvisate" (?)
* cosa significa *improvvisazione*, in un lavoro acusmatico?

## Consegna

* elaborare progetto generale di forma
* elaborare elementi primari della parte 1
* elaborare il concetto d'improvvisazione in un lavoro acusmatico
