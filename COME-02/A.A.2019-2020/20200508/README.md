# Lezione di venerdì 8 maggio 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/znUWLXO9JtA)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica

* Strumentario della composizione musicale elettroacustica:
  * utilizzazione di linguaggi generici (in questo caso: `python`) per la
    realizzazione di partiture `csound`:
    * correzione e completamento del programma `forme.py`

* Discussione dell'ascolto di:

* Schoenberg, *Tre pezzi per pianoforte* op.11 n.2

## Commenti lasciati sulla chat durante la lezione

```chat
02:18:08	Nicola Bernardini:	Philip Glass
02:18:30	Nicola Bernardini:	Steve Reich
02:18:38	Nicola Bernardini:	Alvin Lucier
02:33:50	Nicola Bernardini:	Allen Forte
                              The structure of atonal music
02:55:15	Nicola Bernardini:	Erik Satie
                              Vexations
02:58:51	Nicola Bernardini:	Schoenberg
                              Preludio Op.25 prime 5 battute
```

## Compiti (*cross-fade concretista*)

* Realizzare un programma che realizzi un crossfade discreto secondo lo schema
  che segue:
  ![schema compositivo](./compito_per_il_20200515.png)

* Realizzare un frammento compositivo di un paio di minuti basato su questa
  tecnica di cross-fade, elaborando un set di metadati atto alla realizzazione
  del frammento stesso

### Ascolti

* Arnold Schönberg, *Suite Op.25*, preludio, prime cinque battute
