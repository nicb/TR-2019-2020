# Lezione di venerdì 5 giugno 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/-99NOkwsH9M)

## Argomenti

* Ascolto delle composizioni realizzate con `randomnotes.py`
* Risposta alle eventuali domande

## Realizzazioni degli studenti

### [Realizzazione di Giulio Romano De Mattia](./Giulio/file.md)

### [Realizzazione di Francesco Ferracuti](./Francesco/README.md)

### Composizione Musicale Elettroacustica

* Strumentario della composizione musicale elettroacustica:
  * segmentazioni alternative degli spazi temporali, dinamici, agogici e frequenziali

## Compiti

* modifica di `randomnotes.py` per supporto di spazi segmentati alternativi
