# Lezione di lunedì 9 dicembre 2019

## Argomenti

* campionamento:
  * dinamica del campionamento
    * l'unità di misura (*dB*)
      * come funziona
      * come si usa
    * la dipendenza dal numero di bits del campione
  * quantizzazione
    * l'errore di quantizzazione
    * simulazione di errori di quantizzazione decrescenti: bits = [2 4 8 12 16]

## Lavagne

![whiteboard 1](./TR_II_COME02_19-12-09_17.51.23_1.jpg)

![whiteboard 2](./TR_II_COME02_19-12-09_17.51.23_2.jpg)

## Realizzazioni

Esperimenti con la (simulazione della) quantizzazione nei segnali digitali.

# [Simulazione di un segnale quantizzato a `2 3 4 8 12 16 bit`](./quantizzazione.m)

```matlab
[snd fc] = audioread ("babay.wav");
nbits = [2 3 4 8 12 16];
out = zeros(length(snd)*length(nbits),1);
dur = length(snd)/fc;

for k = 1:length(nbits)
	curbits = 2^nbits(k);
	curstep = 2/curbits ;
	invcurstep = 1/curstep ;
	globstart= (length(snd)*(k-1)) + 1;
	tmpout = round(snd * invcurstep) / invcurstep ;
	out(globstart : globstart + length(tmpout)-1) = tmpout ; 
end
audiowrite("babayriquantizzato.wav",out,fc)
```

Lo script produce il seguente plot:

![segnale riquantizzato a 2 3 4 8 12 16 bit](./quantizzazione.jpg)

Ascolto del rumore di quantizzazione all'interno del segnale riquantizzato.

# [(Simulazione del) calcolo dell'errore di quantizzazione `2 3 4 8 12 16 32 bit` (segnale originale a 16 bit)](./errorediquantizzazione.m)

```matlab
[snd fc] = audioread ("babay.wav");
nbits = [2 3 4 8 12 16 32];
out = zeros(length(snd)*length(nbits),1);
dur = length(snd)/fc;

for k = 1:length(nbits)
	curbits = 2^nbits(k);
	curstep = 2/curbits ;
	invcurstep = 1/curstep ;
	globstart= (length(snd)*(k-1)) + 1;
	tmpout = round(snd * invcurstep) / invcurstep ;
	dif = snd .- tmpout ;
	out(globstart : globstart + length(tmpout)-1) = dif ; 
end
audiowrite("babaydifquant.wav",out,fc)
```

Lo script produce il seguente plot:

![errore di quantizzazione a 2 3 4 8 12 16 32 bit (segnale originale quantizzato a 16 bit)](./errorediquantizzazione.jpg)

Constatazione della progressiva diminuzione dell'errore di quantizzazione con
l'aumentare del numero di bit (sino ad arrivare a 0 con il segnale
riquantizzato a 16 e 32 bit, poiché la quantizzazione originale vizia
l'esperimento).

## Compiti per casa

1. Replicare da zero (senza guardare) gli script prodotti in classe su campionamento e quantizzazione
1. Produrre una cosinusoide campionata a 2 kHz con un glissando che vada da 500 Hz sino a 2500 Hz. Verificare
   i fenomeni di aliasing e foldover

## Riferimenti bibliografici

* Steiglitz, Kenneth *A Digital Signal Processing Primer*
