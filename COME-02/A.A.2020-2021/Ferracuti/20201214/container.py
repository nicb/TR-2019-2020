from PappaPronta.voices.voice import Voice
from PappaPronta.events.csound_event import CsoundEvent

class Container(Voice):

    def generate(self):
        instr=self.parameters['instr']
        tempo=self.parameters['tempo']
        dur=self.parameters['duration']
        rhythm = self.parameters['rhythm']
        freq=self.parameters['frequence']
        ampmod=self.parameters['ampmod']

        end_time = self.at + self.dur
        cur_at = self.at
        cur_tempo = tempo
        while (cur_at < end_time):
            this_tempo = cur_tempo.next(cur_at)
            cur_metro = 60.0/this_tempo
            cur_step = 4.0*rhythm.next(cur_at)*cur_metro
            cur_freq = freq.next(cur_at)
            cur_dur = dur.next(cur_at)
            cur_instr = instr.next(cur_at)
            cur_ampmod=ampmod.next(cur_at)
            if cur_freq<= 400:
                amp=self.fields[0]
            else:
                amp=self.fields[1]
            cur_amp = amp.next(cur_at)

            msg = "; this_tempo: %.4f, cur_metro: %.4f, cur_step: %.4f, " % (this_tempo, cur_metro, cur_step)
            self.events.append(CsoundEvent(cur_at, cur_dur, cur_instr, [cur_amp, cur_freq,cur_ampmod], msg))
            cur_at += cur_step
