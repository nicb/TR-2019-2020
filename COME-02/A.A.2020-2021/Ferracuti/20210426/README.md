# COME/02 - Lezione del 26/04/2021 (Francesco Ferracuti)

## [YouTube Video](https://youtu.be/QjmXrPvGF8c)

## Argomenti

* logistica della collocazione spaziale dei suoni

![delays](./delays.png)


![posizionamento](./posizionamento.png)


![logistica della spazializzazione](./sound_location_logistics.png)
