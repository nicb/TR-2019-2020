fc = 5000;

sinc = 1/fc ;
X = 10;

x = [0 : sinc : X - sinc ];

w1 = 2 * pi * 100 ; 

w2 = w1 ;

a1 = 1;
a2 = 0.2;

y1 = cos( w1 * x)*a1;
y2 = cos(w2 * x)*a2;

y = y1 .* y2 ;

plot(x , y1 , x , y2 , x , y , "b" )
axis([1.1 1.2 -1.2 1.2])

yint = sum(y) / length(x)

lm = 1343;

yint2 = sum(y(1:lm))/lm 
