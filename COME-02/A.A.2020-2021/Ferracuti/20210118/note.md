# Annotazioni Ferracuti

* bravo, utilizzo "stereotipico" di PappaPronta :-)
* Problemi:
  * dinamica troppo ridotta
  * forma inesistente
  * variazioni dinamiche inesistenti
  * elaborazioni ritmiche troppo semplici (troppo ridondanti)
  * scelta logica delle frequenze incomprensibile
  * agogica inesistente
  * timbri troppo "fermi" (molto "elettronici")
