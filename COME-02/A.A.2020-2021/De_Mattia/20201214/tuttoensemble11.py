####################################################################################################################
f"""il programma è progettato con la possibilità di scegliere se scrivere o se lasciar improvvisare un periodo musicale.
    La scrittura si attua con il passaggio di metadati durata (dur) dell'oggetto Periodo e numero di frasi (n_o_f) all'oggetto
    periodo che con la funzione genera_frasi_by_night genera degli oggetti Frase, le quali a loro volta generano semifrasi,
    e queste ultime dei modi_ritmici."""
####################################################################################################################

import pdb
import random

class Nota:
    def __init__(self, at, dur, amp, freq, instr):
        self.at= at
        self.dur= dur
        self.amp= amp
        self.freq= freq
        self.instr= instr
        #self.w_score()
    def w_score(self):
        return "i%d\t%.3f\t%.3f\t%d\t%.3f" % (self.instr, self.at, self.dur, self.freq, self.amp)

class Suddivisione:
    def __init__(self, l):
        self.unit= l
        self.suddivisione=[]

    def suddividi(self):
        a=self.modo_settiminato()
        b=self.modo_sestinato()
        c=self.modo_quintinato()
        d=self.modo_tetrazzato()
        e=self.modo_terzinato()
        f=self.modo_mezzo()

        self.suddivisione=[a,b,c,d,e,f,self.unit]
        n=self.suddivisione
        #print("ARISUDDIVIDI")
        #print(n)
        #print("###################\n\n")
        return n

    def modo(self, div):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/div
            return n


    def modo_mezzo(self):
        n = self.unit/2.0
        return n

    def modo_terzinato(self):
        if self.unit <0.25:
            return self.unit
        else:
            n= self.unit/3.0
            return n

    def modo_tetrazzato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/4.0
            return n

    def modo_quintinato(self):
        if  self.unit<=0.25:
            return self.unit
        else:
            n= self.unit/5.0
            return n

    def modo_sestinato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/6.0
            return n

    def modo_settiminato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n=self.unit/7.0
            return n

class Ritmica:
#c'è un problema con crea_la_scansione_durate, def_suddivisione suddividi e Suddivisione
    def __init__(self,dur=None):

        self.dur = dur
        self.scansione_durate=[]
        self.pattern=[]
        if self.dur == None:
            self.crea_la_scansione_durate()

    def scegli_unita_per_la_suddivisione(self):
        l=random.choice(self.scansione_durate)
        return l

    def crea_la_scansione_durate(self):
        self.scansione_durate.append(0.125)
        l= 0.125
        n= int((4)/l)-1
        for i in range (0, n):
            l+=0.125
            self.scansione_durate.append(l)

        print("SELF.SCANSIONE_DURATE")
        print(self.scansione_durate)
        print("###################\n\n")
        return l

#il pattern trasmette le durate delle note

    def suddivisione_by_night(self):
        s = Suddivisione(self.dur)
        array_di_suddivisione = s.suddividi()
        n= self.genera_un_modo_ritmico(array_di_suddivisione)
        return n

    def suddivisione(self):
        l = self.scegli_unita_per_la_suddivisione()
        #print("UNITA DI SUDDIVISIONE SCELTA")
        #print(l)
        #print("###################\n\n")

        s = Suddivisione(l)
        array_di_suddivisione = s.suddividi()
        n= self.genera_un_modo_ritmico(array_di_suddivisione)
        return n


    def genera_un_modo_ritmico(self,array_di_pesca):
        ll= float(array_di_pesca[6])
        #print("questo è ll")
        #print(ll)
        #print("###################\n\n")
        #print("array di pesca")
        #print(array_di_pesca)
        #print("###################\n\n")

        r= array_di_pesca[random.randint(0,6)]
        #print("primo numero dentro al pattern")
        #print(r)
        #print("###################\n\n")
        self.pattern.append(r)
        #print("pattern with r")
        #print(self.pattern)
        #print("###################\n\n")
        u= float(ll)-float(r)
        #print("u")
        #print(u)
        #print("###################\n\n")

        o = random.uniform(0,1)
        #print("o")
        #print(o)
        #print("###################\n\n")

        while  u > 0:
            #print("pattern a ogni giro")
            #print(self.pattern)
            #print("\n\n")

            #print("u")
            #print(u)
            #print("\n\n")

            #print(sum(self.pattern))
            #print("sum\n\n")
            if o > 0.7:
                if (float(self.pattern[0]) == float(array_di_pesca[0])) or (float(self.pattern[0]) == float(array_di_pesca[1])) or (float(self.pattern[0]) == float(array_di_pesca[2])) or (float(self.pattern[0]) == float(array_di_pesca[4])):
                    while u>0:
                        u= u-r
                        if u>=0:
                            self.pattern.append(r)
                            #print(u)

            j=array_di_pesca[random.randint(0,5)]
            #print("j a questo giro")
            #print(j)

            u= float(u)-float(j)
            if (j + sum(self.pattern)) <= ll:
                self.pattern.append(j)
            elif (j + sum(self.pattern)) > ll:
                j=array_di_pesca[random.randint(0,4)]
                if (j + sum(self.pattern)) <= ll:
                    self.pattern.append(j)
                elif (j + sum(self.pattern)) > ll:
                    j=array_di_pesca[random.randint(0,3)]
                    if (j + sum(self.pattern)) <= ll:
                        self.pattern.append(j)
                    elif (j + sum(self.pattern)) > ll:
                        j=array_di_pesca[random.randint(0,2)]
                        if (j + sum(self.pattern)) <= ll:
                            self.pattern.append(j)
                        else:
                            continue
            else:
                self.pattern.append(j)


        print(self.pattern)
        return self.pattern

class Modo_ritmico(Ritmica):

    cnt = 0
    cont_at = 0

    def __init__(self,dur=None):
        super(Modo_ritmico,self).__init__(dur)
        self.dur = dur
        Modo_ritmico.cnt += 1
        #definire un contatore dentro l'istanza per riconoscere a quale instanza mi riferisco
        self.note = []
        self.cont = Modo_ritmico.cnt
        if self.dur == None:
            self.n = self.suddivisione()
        else:
            self.n = self.suddivisione_by_night()
        self.crea_modo_ritmico()


    def crea_modo_ritmico(self):
        for i in range(0,len(self.n)):
            ninetto_bello = random.randint(1,2)
            if Modo_ritmico.cnt == 1:
                nota =Nota(Modo_ritmico.cont_at, self.n[i], ninetto_bello,1,  1)
                self.note.append(nota)
                Modo_ritmico.cont_at += self.n[i]
            else:
#AIAIAIAIAIAIAAIAIAIIIIII
#prendere ultima nota dell'ultimo modo ritmico generato e fare somma di self.at e self.dur
                nota = Nota(Modo_ritmico.cont_at,self.n[i],ninetto_bello,1,1)
                self.note.append(nota)
                Modo_ritmico.cont_at+=self.n[i]

            #print("note")
            #print(nota)

    def to_csound(self):
        for g in self.note:
            print(g.w_score())

class Semifrase():

    cnt = 0

    def __init__(self,dur=None, n_o_mr=None):
        self.dur = dur
        self.n_o_mr= n_o_mr
        self.modi_ritmici=[]
        Semifrase.cnt += 1
        if self.dur == None:
            self.crea_una_semifrase()
        else:
            self.crea_una_semifrase_by_night()


    def crea_una_semifrase(self):
        for i in range(0,2):
            n= Modo_ritmico()
            #print("modi ritmici")
            #print(n)
            self.modi_ritmici.append(n)

    def crea_una_semifrase_by_night(self):
        for i in range(0,self.n_o_mr):
            n= Modo_ritmico(self.dur/2.0)
            self.modi_ritmici.append(n)

    def to_csound(self):
        for g in self.modi_ritmici:
            g.to_csound()

class Frase():

    def __init__(self,dur=None,u_o_s=None):
        self.dur = dur
        self.u_o_s= u_o_s
        self.semifrasi=[]
        if self.dur == None:
            self.crea_una_frase()
        else:
            self.crea_una_frase_by_night()

    def crea_una_frase(self):
        for i in range(0,2):
            n= Semifrase()
            #print("semifrasi")
            #print(n)
            self.semifrasi.append(n)

    def crea_una_frase_by_night(self):
        for i in range(0,3):
            n = Semifrase(self.dur/3.0,2)
            self.semifrasi.append(n)

    def to_csound(self):
        for g in self.semifrasi:
            g.to_csound()

class Periodo():
    def __init__(self,dur=None,n_o_f=None):
        self.dur = dur
        self.frasi=[]
        self.n_o_f=n_o_f
        if self.dur== None:
            self.genera_frasi()
        else:
            self.genera_frasi_by_night()
        self.to_csound()

    def genera_frasi(self):
        for i in range(0,3):
            n = Frase()
            #print("frasi")
            #print(n)
            self.frasi.append(n)

#sceglie i periodi di frasi
    def genera_frasi_by_night(self):
        o = random.uniform(0,1)
        print(o)
        print("o")
        print("#########")
        len_normal = float(self.dur) / float(self.n_o_f)
        if o > 0.45:
            n = len_normal
            l = len_normal+(len_normal/5.0)
            b = len_normal-(len_normal/5.0)
            if self.n_o_f == 2:
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(l,3))
            if self.n_o_f == 3:
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(b,2))
            if self.n_o_f == 4:
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(b,2))

            if self.n_o_f == 5:
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(n,2))
        else:
            for i in range(0,self.n_o_f):
                self.frasi.append(Frase(len_normal,2))


    def progetta_la_frase(self):
        pass



    def to_csound(self):
        print("f1 0 4096 10 1")
        for g in self.frasi:
            g.to_csound()

v=Periodo()

################################################
#ATTENZIONE
################################################
#SISTEMARE SCANSIONE DURATE ECC IN RITMICA E IN MODO ritmico
#aggiungere Compositore: ci sono diversi modi ritmici da scegliere, compositore sceglie il modo ritmico che piu si adatta in base al modo ritmico,semifrase,frase precedenti
#INTEGRARE ACCORDI
#sviluppare il sistema per la frequenza
