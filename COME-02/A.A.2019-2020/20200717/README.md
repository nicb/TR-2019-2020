# Lezione di venerdì 17 luglio 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/kJyi90doVy0)

## Argomenti

### Discussioni degli esercizi compositivi

#### [Giulio Romano De Mattia](./Giulio/README.md)

#### [Francesco Ferracuti](./Francesco/README.md)

## Compiti

* Scrittura di un testo con la dichiarazione di intenti per il prossimo/attuale esercizio
  compositivo da inviare al docente entro il **TERMINE IMPROROGABILE** di
  giovedì 23 luglio 2020 alle ore 18:00.
* acquisire l'expertise necessaria ad usare **proficuamente** il software `git` (seguirà
  verifica sul livello acquisito)
