#python
from random import randint, random
n_note=100
n=0
at=0
print("f1 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111\nf2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14")
while (n<n_note):
    instr=randint(1,4)
    step=random()*0.2
    dur=random()*0.5+0.10
    freq=random()*200+150
    amp=random()*-6-12
    fun=randint(1,2)
    print("i%d %8.4f %8.4f %+8.4f %8.4f %d" % (instr, at, dur, amp, freq, fun))
    at=at+step
    n=n+1
