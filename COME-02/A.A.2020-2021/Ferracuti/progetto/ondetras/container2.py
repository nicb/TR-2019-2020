from PappaPronta.voices.voice import Voice
from PappaPronta.events.csound_event import CsoundEvent

class Container2(Voice):

    def generate(self):
        instr=self.parameters['instr']
        att=self.parameters['ats']
        dur=self.parameters['duration']
        amposc=self.parameters['amposc']
        ampmod=self.parameters['ampmod']
        freqosc=self.parameters['freqosc']
        freqmod=self.parameters['freqmod']
        end_time = self.at + self.dur
        cur_at = self.at
        while (cur_at < end_time):
            cur_dur = dur.next(cur_at)
            cur_instr = instr.next(cur_at)
            cur_amposc = amposc.next(cur_at)
            cur_ampmod = ampmod.next(cur_at)
            cur_freqosc = freqosc.next(cur_at)
            cur_freqmod= freqmod.next(cur_at)
            freq=self.field[0]
            cur_freq=freq.next(cur_at)
            #msg = "; this_tempo: %.4f, cur_metro: %.4f, cur_step: %.4f, " % (this_tempo, cur_metro, cur_step)
            self.events.append(CsoundEvent(cur_at, cur_dur, cur_instr, cur_amposc, cur_ampmod,cur_freqosc,cur_freqmod,[cur_freq]))
            cur_at += cur_dur
