sr=44100
ksmps=10
nchnls=1

instr 1
iamp=ampdbfs(p5)
ifreq=p4
idur=p3

kenv expseg iamp*0.01,idur*0.1,iamp,idur*0.99,iamp*0.001
aout oscil kenv,ifreq,1
     out aout
endin
