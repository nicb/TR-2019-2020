# Lezione di mercoledì 20 novembre 2019

## Argomenti

* ritiri: **UFFICIALIZZARE** i ritiri se accadono
* ascolto: 2 ore di Radio 5 al giorno
* strumenti
  * [`octave`](https://octave.org)
  * [`csound`](https://csounds.com)
  * [`pure data`](http://pure-data.info)
* linguaggi
  * compilatore `C` (`gcc`)
  * [`python`](https://python.org)
  * [`ruby`](https://www.ruby-lang.org/it/)
* seminario di programmazione
  * inizio il 9 dicembre alle ore 14:00

## Lezione

* Prima introduzione a `octave`
  * calcolo vettoriale - operazioni di base
  * funzioni trigonometriche: `cos`, `sin`, etc.
  * help in linea
  * plot di funzioni
  * verifica
