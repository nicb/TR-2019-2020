import pdb

from PappaPronta.voices.voice import Voice
from PappaPronta.patterns.all_patterns import *
from container import Container

class SuperContainer(Voice):

    def generate(self):
        nevs = self.parameters['sub_events'].next(0)
        instr=self.parameters['instr']
        ats = self.parameters['ats']
        durs = self.parameters['durs']
        tempos = self.parameters['tempos']
        rhythms = self.parameters['rhythms']
        durations = self.parameters['durations']
        ampmods = self.parameters['ampmods']
        frequencies = self.parameters['frequencies']
        fields = self.parameters['fields']

        end_time = self.at + self.dur
        cur_at = ats.next(self.at)
        cur_tempo = tempos.next(cur_at)
        id = 1
        while (id <= nevs):
            this_tempo = cur_tempo.next(cur_at)
            sub_params = { 'id': id, 'instr': instr.next(cur_at), 'tempo': this_tempo,
                    'rhythm': rhythms.next(cur_at), 'duration': durations.next(cur_at), 
                    'ampmod': ampmods.next(cur_at).next(cur_at), 'frequence':
                    frequencies.next(cur_at).next(cur_at) }
            sub_fields = [ fields.next(cur_at), fields.next(cur_at) ]
            this_at = cur_at
            this_dur = durs.next(cur_at)
            this_cls = Container
            sub_dict = { 'class': this_cls, 'at': this_at, 'dur': this_dur, 'parameters': sub_params, 'fields': sub_fields }
            msg = "; Container %03d this_tempo: %.4f" % (id, this_tempo)
            self.events.append(this_cls.create(sub_dict))
            cur_at = ats.next(cur_at)
            id += 1

    #
    # FIXME:
    # The parse_dictionary and parse_value have to be rewritten in order to be
    # able to do sub-sequencing. This might be a feature to be incorporated
    # into the main library, as it could be very useful. We test it here first
    # 
    # In the 'parse_dictionary' call we just modify the call to 'Voice' to the
    # call to 'cls'
    #
    @classmethod
    def parse_dictionary(cls, dict):
        at = dict['at']
        dur = dict['dur']
        pars = {}
        flds = []
        for k, v in dict['parameters'].items():
            val = cls.parse_value(v, at, dur) 
            pars[k] = val
        #pdb.set_trace()
        # flds = [cls.parse_value(v, at, dur) for v in dict['fields']]
        return cls(at, dur, dict['class'], pars, flds)

    #
    # this is trickier. We need to evaluate separately every item of the
    # argument (sargs) and rebuild an array on it with the following rules:
    # - every element is converted to an individual string
    # - if it converts to a number, then it is converted into a Constant(at, dur, value))
    # - if it is something else, then it gets scanned again to be evaluated
    #
    @classmethod
    def parse_value(cls, v, at, dur):
        (kname, sargs) = v.split('(', 1)
        klass = eval(kname)
        vargs = cls.parse_argument(kname, sargs, at, dur)
        return klass(at, dur, vargs)

    #
    # FIXME: split should be done with regexp
    #
    @classmethod
    def parse_argument(cls, kname, arg, at, dur):
        res = None
        pdb.set_trace()
        arg = arg[:-1]               # remove the last parenthesis
        if (kname == 'Constant'):
            res = eval(arg)             # result will be a scalar
        else:
            res = []                    # result will be an array
            arg = arg[1:-1]             # remove the first and last square bracket
            el_strings = arg.split('),') # split in a collection of element strings
            el_strings[:-1] = [x + ')' for x in el_strings[:-1]] # GRRRR I have to add the parenthesis back
            atd_string = f'{at}, {dur}, '
            for el_s in el_strings:        # evaluate every part separately
                v = None
                try:
                  v = float(el_s)       # we try to convert directly into float
                except ValueError:      # if it's not a float, we reiterate the evaluation
                  el_s = el_s.replace('(', '('+ atd_string)
                  v = eval(el_s)
                res.append(v)
        return res

    #
    # FIXME: this should go back into the library and should be implemented
    #
    def check_time(self, t):
        return True
