from yaml import load,dump
import pdb
def read(file):
    yaml_file=None
    with open(file,"r")as fh:
        yaml_file=load(fh)
    return yaml_file

def write(config):
    for k,v in config.items():
        file_name=str(k)+".yml"
        single={k: v}
        with open(file_name,"w")as fh:
            fh.write(dump(single))

if __name__=="__main__":
    #pdb.set_trace()
    cfg=read("../conf_template.yml")
    write(cfg)
