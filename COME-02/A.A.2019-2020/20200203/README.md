# Lezione di lunedì 03 febbraio 2020

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica (2 ore)

* Motivi di fondo della musica elettroacustica:
  * musica come ricerca e speculazione intellettuale
  * emergenza del timbro
  * l'elettroacustica come utopia

#### Compiti per casa

* Ideare un breve frammento *musicale* con spazi non segmentati
  convenzionalmente

### Elaborazione Numerica dei Segnali (2 ore)

* Analisi della forma d'onda
  * *Discrete Fourier Transform* (*DFT*)
    * realizzazioni in `octave`:
      * segnale complesso, frequenza "non-intonata"
      * compromesso tempo-frequenza
      * passaggio da segnale complesso a segnale reale

### Lavagna

![whiteboard](./TR_I_2020-02-03_18.26.43_1.jpg)

### Scripts `octave`

#### [`dft1.m`: (ex-compito per casa) somma di due funzioni complesse (*bin-synchronous*)](./dft1.m)

In questo script si sommano due frequenze (*bin-synchronous*) una multipla dell'altra
e se ne fa la *Discrete Fourier Transform* (*DFT*).


```matlab
fc = 5000;
sinc = 1/fc;
freq = 500;
dur = 2;
t = [0 : sinc : dur - sinc];
amp = 0.8;
amp2 = 0.2;

y = amp*e.^(i*2*pi*freq*t) + amp2*e.^(i*2*pi*freq*3*t);

nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end

figure(1)
mag = abs(ris)/length(t);
stem(F, mag)

figure(2)
plot(t, real(y), t, imag(y))
axis([0.39 0.41])

figure(3)
plot(t, abs(y))
axis([0.39 0.41])
```

Questo script produce i seguenti grafici:

![risultato della *DFT*](./dft1_1.jpg)

![parte reale e parte immaginaria nel dominio del tempo](./dft1_2.jpg)

![modulo della funzione](./dft1_3.jpg)

#### [`dft2.m`: *DFT* di una funzione reale](./dft2.m)

Lo script [`dft2.m`](./dft2.m) realizza la *Discrete Fourier Transform*
(*DFT*) di una funzione reale non *bin-synchronous*:


```matlab
fc = 5000;
sinc = 1/fc;
freq = 106.2;
dur = 0.1;
t = [0 : sinc : dur - sinc];
amp = 0.8;

y = amp*cos(2*pi*freq*t);

nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
figure(1)
mag = abs(ris)/length(t);
stem(F, mag)
axis([-freq*2 freq*2 0 0.5])

figure(2)
plot(t, y)
axis([0.015 0.045])
```

Lo script produce is seguenti grafici:

![funzione reale nel dominio del tempo](./dft2_1.jpg)

![*dft* risultante](./dft2_2.jpg)

Si può constatare che, rispettando la formula di Eulero (![cos
eulero](./cos_eulero.png)), la *DFT* di una funzione reale
produce due funzioni complesse e coniugate i cui moduli valgono la metà
dell'ampiezza della funzione reale.

#### [`dft3.m`: *DFT* di una funzione reale - definizione analitica migliore](./dft3.m)

```matlab
fc = 5000;
sinc = 1/fc;
freq = 106.2;
dur = 50/freq;
t = [0 : sinc : dur - sinc];
amp = 0.8;

y = amp*cos(2*pi*freq*t);

binsize = 0.1;
nfreq = fc/binsize;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
figure(1)
mag = abs(ris)/length(t);
stem(F, mag)
axis([-freq*2 freq*2 0 0.45])

figure(2)
stem(t, real(y))
axis([dur-20*sinc dur])
```
Lo script produce is seguenti grafici:

![*dft* risultante](./dft3_1.jpg)

![funzione reale nel dominio del tempo - ultimi campioni](./dft3_2.jpg)

#### Compiti per casa

* sintesi e analisi di una funzione reale con più componenti
