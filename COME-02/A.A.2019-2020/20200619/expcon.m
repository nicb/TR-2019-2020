function y=expcon(x,y_start,y_end,c,tau,x_len)
  v_start=y_start-c-tau;
  v_end=y_end-c-tau;
  a=(log(v_end)-log(v_start))/x_len;
  b=log(v_start);
  y=exp(a*x+b)+c+tau;
end
