class Periodo():
    def __init__(self,dur=None,n_o_f=None):
        self.dur = dur
        self.frasi=[]
        self.n_o_f=n_o_f
        if self.dur== None:
            self.genera_frasi()
        else:
            self.genera_frasi_by_night()
        self.to_csound()

    def genera_frasi(self):
        for i in range(0,3):
            n = Frase()
            #print("frasi")
            #print(n)
            self.frasi.append(n)

#sceglie i periodi di frasi
    def genera_frasi_by_night(self):
        o = random.uniform(0,1)
        #print(o)
        #print("o")
        #print("#########")
        len_normal = float(self.dur) / float(self.n_o_f)
        if o > 0.45:
            n = len_normal
            l = len_normal+(len_normal/5.0)
            b = len_normal-(len_normal/5.0)
            if self.n_o_f == 2:
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(l,3))
            if self.n_o_f == 3:
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(b,2))
            if self.n_o_f == 4:
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(b,2))

            if self.n_o_f == 5:
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(b,2))
                self.frasi.append(Frase(n,2))
                self.frasi.append(Frase(l,3))
                self.frasi.append(Frase(n,2))
        else:
            for i in range(0,self.n_o_f):
                self.frasi.append(Frase(len_normal,2))


    def progetta_la_frase(self):
        pass



    def to_csound(self):
        print("f1 0 4096 10 1")
        print("f2 0 4096 10 1 0.8 0.6 0.2 0.1")
        for g in self.frasi:
            g.to_csound()

