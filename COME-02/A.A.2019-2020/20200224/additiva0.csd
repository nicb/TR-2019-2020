<CsoundSynthesizer>
<CsOptions>
-o "additiva.wav" -W
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1



instr 1
kfreq oscil 20, 6, 1
kfreq = kfreq + p5

a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 0.05*p3, p3, 0.05*p3
out a1
endin

</CsInstruments>
<CsScore>

f1 0 4096 10 1                                       ;sinusoide
f2 0 4096 10 1 0.5 0.33  0.25 0.2  0.16  0.14        ;dente di sega 
f3 0 4096 10 1 0   0.33  0    0.16 0     0.14 0 0.111;quadra
f4 0 4096 10 1 0   -0.11 0    0.04 0    -0.02        ;triangolare

i1 0 1 -3 500 1
i1 2 1 -3 500 2
i1 4 1 -3 500 3
i1 6 1 -3 500 4

; prendere un suono o pi� suoni concreti e mescolare con suoni sintetici/zzati, ci� che si vuole mettere in risalto � il "dove tailing" ovvero la transizione dal suono concreto al suono sintetico.

</CsScore>
</CsoundSynthesizer>
