# Lezione di venerdì 15 maggio 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/B19IXQIQpzM)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica

* Strumentario della composizione musicale elettroacustica:
  * introduzione alla realizzazione di partiture grafiche con `pic` (`groff suite`)

* Discussione dell'ascolto di:

* Schoenberg, *Suite op.25* preludio, prime 5 battute

### Ascolti per la prossima volta

* Schoenberg, op.33a n.1
