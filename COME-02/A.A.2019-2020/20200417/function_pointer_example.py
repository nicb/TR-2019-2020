#
# Definisco una classe con un certo numero di metodi diversi
# 
class Slave:

    def __init__(self, name):
        self.name = name

    def i_am(self, f):
        print "I am the function %s of the object %s" % (f, self.name)

    def function_a(self):
        self.i_am('a')

    def function_b(self):
        self.i_am('b')

    def function_c(self):
        self.i_am('c')

#
# questa funzione chiama un metodo di un'istanza di una classe che
# si trova all'interno di una collezione
#

def runner(objs, function):
    for o in objs:
       function(o)

#
# array di istanze della classe Slave
#
ss = [ Slave('s0'), Slave('s1'), Slave('s2') ]

#
# chiamata dei vari metodi della classe slave a turno 
#
runner(ss, Slave.function_c)
runner(ss, Slave.function_b)
runner(ss, Slave.function_a)
