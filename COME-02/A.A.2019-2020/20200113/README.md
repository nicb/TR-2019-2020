# Lezione di lunedì 16 dicembre 2019

## Argomenti

* Correzione dei compiti
* Risposta alle eventuali domande
* Memorandum sui numeri complessi:
  * definizione
  * proprietà
  * uso del numero di Nepero
  * formula di Eulero

## Lavagne

![whiteboard 1](./TR_I_2020-01-13_17.26.08_1.jpg)

![whiteboard 2](./TR_I_2020-01-13_17.26.08_2.jpg)
