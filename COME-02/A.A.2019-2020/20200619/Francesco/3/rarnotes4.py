import random
import re
class Nota:
    def __init__(self,at,dur,freq,amp,instr,table):
        self.at=at
        self.dur=dur
        self.amp=amp
        self.freq=freq
        self.instr=instr
        self.table=table

    def to_csound(self):
        a="i%d %8.4f %8.4f %8.4f %8.4f %8.4f\n" %(self.instr,self.at,self.dur,self.freq,self.amp,self.table)
        return a

class Generatore:
    def __init__(self,num_note,at,dur,fmin,fmax,number):
        self.at=at
        self.dur=dur
        self.fmin=fmin
        self.fmax=fmax
        self.number=number
        self.note=[]
        self.num_note=num_note
        self.frequenze=self.genera_frequenze()
        self.genat=self.genera_at()
    def __repr__(self):
        return "Test at: %s" %(self.genat)
    def generate(self):
        for x in range(1,self.num_note):
            at=self.calc_at()
            dur=self.calc_dur()
            freq=self.calc_freq()
            amp=self.calc_amp()
            table=self.calc_table()
            instr=1
            n=Nota(at,dur,freq,amp,instr,table)
            self.note.append(n)

    def genera_at(self):
        at=[]
        if self.number==1:
            for n in range (2,100):
                at.append(self.dur-((self.dur)/n))
        else:
            for n in range (2,100):
                at.append(((self.dur)/n)+self.at)
        return at

    def genera_frequenze(self):
        result=[self.fmin]
        frange=self.fmax-self.fmin
        for n in range(1,100):
            result.append(self.fmin+(frange*n/4))
        result.append(self.fmax)
        return result
    def calc_at(self):
        sz=len(self.genat)
        at=self.genat[random.randrange(0,sz)]
        return at
    def calc_dur(self):
        dur=random.uniform(0.5,2)
        return dur
    def calc_freq(self):
        sz=len(self.frequenze)
        r=self.fmax-self.fmin
        freq=self.frequenze[random.randint(0,sz-1)]
        return freq
    def calc_amp(self):
        amp=-24
        return amp
    def calc_table(self):
        if self.number==1:
            self.table=1
        else:
            self.table=2
        return self.table
    def to_csound(self):
        str=""
        for x in self.note:
            str+=x.to_csound()
        return str
class Composizione:
    def __init__(self,file):
        self.file=file
        self.generatori=[]
        self.create()

    def create(self):
        f=open(self.file,"r")
        lines=f.readlines()
        for l in lines:
            if re.match(r'^\s*#',l):
                continue
            (nn,at,dur,fmin,fmax,number)=l.split(" ")
            self.generatori.append(Generatore(int(nn),float(at),float(dur),float(fmin),float(fmax),int(number)))
        f.close()

    def to_csound(self):
        print("f1 0 4096 10 1\nf2 0 16384 10 1 0   0.3 0    0.2 0     0.14 0     .111 ")
        for g in self.generatori:
            g.generate()
            print(g.to_csound())
c=Composizione("file3.meta")
c.to_csound()
