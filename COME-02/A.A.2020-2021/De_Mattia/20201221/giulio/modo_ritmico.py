from ritmica import Ritmica

class Modo_ritmico(Ritmica):

    cnt = 0
    cont_at = 0

    def __init__(self,dur=None):
        super(Modo_ritmico,self).__init__(dur)
        self.dur = dur
        Modo_ritmico.cnt += 1
        #definire un contatore dentro l'istanza per riconoscere a quale instanza mi riferisco
        self.note = []
        self.cont = Modo_ritmico.cnt
        if self.dur == None:
            self.n = self.suddivisione()
        else:
            self.n = self.suddivisione_by_night()
        self.crea_modo_ritmico()


    def crea_modo_ritmico(self):
        for i in range(0,len(self.n)):
            ninetto_bello = random.randint(1,2)
            if Modo_ritmico.cnt == 1:
                nota =Nota(Modo_ritmico.cont_at, self.n[i], ninetto_bello,1,  1)
                self.note.append(nota)
                Modo_ritmico.cont_at += self.n[i]
            else:
#AIAIAIAIAIAIAAIAIAIIIIII
#prendere ultima nota dell'ultimo modo ritmico generato e fare somma di self.at e self.dur
                nota = Nota(Modo_ritmico.cont_at,self.n[i],ninetto_bello,1,1)
                self.note.append(nota)
                Modo_ritmico.cont_at+=self.n[i]

            #print("note")
            #print(nota)

    def to_csound(self):
        for g in self.note:
            print(g.w_score())

