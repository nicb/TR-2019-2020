# Lezione di venerdì 20 marzo 2020 (lezione svolta in remoto via zoom)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

### [Correzione dei compiti (Realizzazione in classe)](./lezione.csd)

```csound
<CsoundSynthesizer>
<CsOptions>
-Wo "lezione0.wav"
</CsOptions>
<CsInstruments>

sr     = 44100
ksmps  = 32
nchnls = 1
0dbfs  = 1

instr 1
ifreq = p4
iamp  = p5
a1 soundin "signal.wav"
asint oscil iamp, ifreq, 1
asint linen asint, p3*0.3, p3, p3*0.05             		 
out (a1+asint)/2
endin

</CsInstruments>
<CsScore>
f1 0 4096 10 1
i1 0 2 602.8 0.7

</CsScore>
</CsoundSynthesizer>
```

```sh
$ csound lezione.csd
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
UnifiedCSD:  lezione.csd
STARTING FILE
Creating options
Creating orchestra
closing tag
Creating score
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
graphics suppressed, ascii substituted
0dBFS level = 1.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to lezione0.wav (WAV)
SECTION 1:
ftable 1:
ftable 1:	4096 points, scalemax 1.000
               .-''''''-._
            _-'           '.
          _-                '.
         -                    '_
       .'                       -
      -                          '_
    _'                             .
   .                                -
  -                                  '_
_'_____________________________________._______________________________________
                                        -                                     .
                                         -                                   -
                                          '_                               _'
                                            .                             .
                                             -                           -
                                              '.                       _'
                                                -_                    -
                                                  -_                .'
                                                    -_           _.'
                                                      '-._____..'
new alloc for instr 1:
diskin2: opened 'signal.wav':
         44100 Hz, 1 channel(s), 48384 sample frames

B  0.000 ..  2.000 T  2.000 TT  2.000 M:  0.83832
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  0.83832
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.006s, CPU: 0.006s
256 512 sample blks of shorts written to lezione0.wav (WAV)
```

Il file `lezione0.wav` risultante produce il grafico che segue:

![lezione0.wav](./lezione0.jpg)

#### Compiti per casa

* Elaborare musicalmente quanto fatto in classe

### Elaborazione Numerica dei Segnali (2 ore)

* Analisi della forma d'onda
  * *Discrete Fourier Transform* (*DFT*)
    * realizzazioni in `octave`:
      * funzione della finestratura
      * finestre cosinusoidali vs. finestre rettangolari
      * compromesso tempo-frequenza

#### Lavagne

![whiteboard 1](./TR_I_20200320_1.png)

![whiteboard 2](./TR_I_20200320_2.png)

#### Codice `octave` prodotto in classe

##### [finestratura rettangolare](./modamp1.m)

```matlab
fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=0.8*cos(2*pi*freqp*t)+0.3*cos(2*pi*freqp*2*t);
nsamples=length(t);
modu=[ones(1,nsamples/4) zeros(1,nsamples/2) ones(1,nsamples/4)];
res=port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.3 1.3])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -0.3 1.3])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.3 1.3])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 300 -0.005 0.21])
```

Questo codice produce i grafici che seguono:

![modamp1 1](./modamp1_1.jpg)

![modamp1 2](./modamp1_2.jpg)

##### [finestratura cosinusoidale](./modamp2.m)

```matlab
fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=0.8*cos(2*pi*freqp*t)+0.3*cos(2*pi*freqp*2*t);
nsamples=length(t);
freqm=1/dur;

modu= -0.5*cos(t*2*pi*freqm)+0.5;
res = port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.3 1.3])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -0.3 1.3])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.3 1.3])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 300 -0.005 0.21])
```

Questo codice produce i grafici che seguono:

![modamp2 1](./modamp2_1.jpg)

![modamp2 2](./modamp2_2.jpg)

#### Compiti per casa

* scomposizione di segnali reali armonici e inarmonici
