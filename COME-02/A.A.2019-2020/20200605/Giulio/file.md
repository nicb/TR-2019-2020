# DESCRIZIONE E ANALISI ESERCIZIO RANDOMNOTES

[il programma che abbiamo scritto](./randomnotesa.py) (vedi sotto) genera degli spazi sonori di forma rettangolare
all'interno di un grafico bidimensionale tempo-frequenza.

Il soggetto che ho scelto per questo esercizio è [un quadro di Theo Van Doesburg](./quadro.jpg), un pittore
astrattista che in questo caso impiega un linguaggio grafico interessante poiché l'opera è costituita
da un insieme di rettangoli di vari colori e di lunghezza variabile disposti orizontalmente
e verticalmente su una superficie in modo da generare delle tensioni. Anche la superficie è
divisa tra una zona grigia e una zona bianca.

![quadro](./quadro.jpg)

Ho scelto di ruotare il quadro di 90 gradi e di rappresentare i rettangoli attraverso i generatori.
Successivamente ho scelto il numero di note secondo alcune prove che ho svolto generando il file.wav
e controllando lo spettro sonoro per identificare la densità di note in ogni evento sonoro.

La densità rappresenta il colore dei rettangoli secondo lo schema crescente dello spettro luminoso.

![disegno](./disegno.jpg)

## [`randomnotesa.py`](./randomnotesa.py)

```python
import random
class Nota:
    def __init__(self,at,dur,freq,amp,instr):
        self.at=at
        self.dur=dur
        self.amp=amp
        self.freq=freq
        self.instr=instr

    def to_csound(self):
        a="i%d %8.4f %8.4f %8.4f %8.4f\n" %(self.instr,self.at,self.dur,self.freq,self.amp)
        return a

class Generatore:
    def __init__(self,num_note,at,tf,fmin,fmax):
        self.at=at
        self.tf=tf
        self.fmin=fmin
        self.fmax=fmax
        self.note=[]
        self.num_note=num_note
    def generate(self):
        for x in range(1,self.num_note):
            at=self.calc_at()
            dur=self.calc_dur(at)
            freq=self.calc_freq()
            amp=self.calc_amp()
            instr=1
            n=Nota(at,dur,freq,amp,instr)
            self.note.append(n)
    def calc_at(self):
        at= random.uniform(self.at,self.tf)
        return at
    def calc_dur(self,at):
        dur=random.uniform(0.127,self.tf-at)
        return dur
    def calc_freq(self):
        r=self.fmax-self.fmin
        freq=random.random()*r+self.fmin
        return freq
    def calc_amp(self):
        amp=-32
        return amp       
    def to_csound(self):
        str=""
        for x in self.note:
            str+=x.to_csound()
        return str


class Composizione:
    def __init__(self,file):
        self.file=file
        self.generatori=[]
        self.create()

    def create(self):
        f=open(self.file,"r")
        lines=f.readlines()
        for l in lines:
            (nn,at,tf,fmin,fmax)=l.split(" ")
            self.generatori.append(Generatore(int(nn),float(at),float(tf),float(fmin),float(fmax)))
        f.close()

    def to_csound(self):
        print("f1 0 4096 10 1")
        for g in self.generatori:
            g.generate()
            print(g.to_csound())
c=Composizione("file.meta")
c.to_csound()
```
