fc = 20000;
dur = 0.2;
sinc = 1/fc;

t = [0: sinc: dur-sinc];

fase = pi/4.2;
freq = 100;

y1 = cos(freq*2*pi*t+fase);
y2 = cos(fase)*cos(freq*2*pi*t)-sin(fase)*sin(freq*2*pi*t); 		%formula di Werner

subplot(2,1,1)
plot(t,y1)

subplot(2,1,2)
plot(t,y2)