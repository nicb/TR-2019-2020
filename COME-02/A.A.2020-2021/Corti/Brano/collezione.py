from datetime import datetime
from random import randint, randrange, seed
from frammento import Frammento
class Collezione: 
    def __init__(self,file):
        self.set = Frammento.collezione(file)
        self.indice = 0 
	self.setup_rand()
    
    def scegli(self,indice):
        res = self.set[indice]
        self.indice = indice
        return res
		
    def prossimo(self):
        res = self.scegli(self.indice)
        self.indice += 1
        self.indice = self.indice % len(self.set)
        return res
    
    def prossimo_gruppo(self,fragnum = 2):
	gruppo = [self.prossimo() for n in range(fragnum)] 
	return gruppo


    def a_caso(self):
        indice = randint(0,len(self.set)-1)
        return self.scegli(indice)

    def setup_rand(self):
	self.seed = randrange(0,1e9)
	seed(self.seed)
	with open("seeds.txt","a") as file:
		file.write("%s: %d\n" % (datetime.now(),self.seed))
	
