[y fc]=audioread("didjeridoo.wav");
y=y';
start=20000;
nsamples=10000;
dur=length(y)/fc;
sinc=1/fc;
t=[0:sinc:dur-sinc];
modamp=zeros(1, length(y));
modamp(start:start+nsamples-1)=ones(1, nsamples);

result=y.*modamp;
subplot(3,1,1)
plot(t,y)
subplot(3,1,2)
plot(t,modamp)
subplot(3,1,3)
plot(t,result)

